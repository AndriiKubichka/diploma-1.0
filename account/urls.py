from django.conf.urls import url
from . import views
from django.urls import path, include
from django.contrib.auth.views import LoginView, LogoutView

urlpatterns = [
#    path('login/', views.user_login, name='login'),
#    path('logout/', views.user_logout, name='login'),
    path('login/', LoginView.as_view(template_name='login.html'), name='login'),
    path('logout/', LogoutView.as_view(template_name='logout.html'), name='logout'),
    #path('logout-then-login/', django.contrib.auth.views.logout_then_login, name='logout_then_login'),
    path('', views.dashboard, name='dashboard')
]