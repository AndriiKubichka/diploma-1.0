from django.contrib import admin
from .models import Clients
from .models import ClientUsers
from .models import Materials
from .models import Orders
from .models import OrderLines
from .models import Profiles, ProfileTypes
from .models import StabdardRows
from .models import DeliveryOrders
from .models import DeliveryOrderLines
from .models import Nomenclatures
from .models import Categories

# Register your models here.

admin.site.register(Clients)
admin.site.register(ClientUsers)
admin.site.register(Materials)
admin.site.register(Orders)
admin.site.register(Profiles)
admin.site.register(OrderLines)
admin.site.register(ProfileTypes)
admin.site.register(StabdardRows)
admin.site.register(DeliveryOrders)
admin.site.register(DeliveryOrderLines)
admin.site.register(Nomenclatures)
admin.site.register(Categories)
