# Generated by Django 2.2.7 on 2021-01-08 13:29

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0024_auto_20210106_1653'),
    ]

    operations = [
        migrations.AddField(
            model_name='clients',
            name='Password',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='orders',
            name='SysDate',
            field=models.DateField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
    ]
