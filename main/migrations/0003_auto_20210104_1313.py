# Generated by Django 2.2.7 on 2021-01-04 11:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_auto_20201221_2134'),
    ]

    operations = [
        migrations.AddField(
            model_name='clientusers',
            name='Password',
            field=models.CharField(default='', max_length=50),
        ),
        migrations.AddField(
            model_name='clientusers',
            name='UserName',
            field=models.CharField(default='', max_length=50),
        ),
    ]
