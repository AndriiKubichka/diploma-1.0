# Generated by Django 2.2.7 on 2021-01-05 13:15

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0010_auto_20210105_1514'),
    ]

    operations = [
        migrations.AlterField(
            model_name='orders',
            name='Material1',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='main.Materials'),
            preserve_default=False,
        ),
    ]
