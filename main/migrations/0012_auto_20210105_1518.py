# Generated by Django 2.2.7 on 2021-01-05 13:18

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0011_auto_20210105_1515'),
    ]

    operations = [
        migrations.AlterField(
            model_name='orders',
            name='Material1',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.Materials'),
        ),
    ]
