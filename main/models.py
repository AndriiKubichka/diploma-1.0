from django.db import models
from django.conf import settings
from django.utils.translation import ugettext as _


# Create your models here.

class IntegerRangeField(models.IntegerField):
    def __init__(self, verbose_name=None, name=None, min_value=None, max_value=None, **kwargs):
        self.min_value, self.max_value = min_value, max_value
        models.IntegerField.__init__(self, verbose_name, name, **kwargs)
    def formfield(self, **kwargs):
        defaults = {'min_value': self.min_value, 'max_value':self.max_value}
        defaults.update(kwargs)
        return super(IntegerRangeField, self).formfield(**defaults)


class Clients(models.Model):
    FORMOFPAYMENT = (('CP', 'CP'),
                    ('BT', 'BT'))

    SHIPPING = (
        (0, "За замовчуванням"),
        (1, "Відправка клієнту"),
        (5, "Самовивіз"),
    )

    Name = models.CharField(max_length = 150)
    FullName = models.CharField(max_length = 500)
    EDRPOU = models.CharField(max_length = 10)
    Password = models.CharField(max_length = 20, blank=True, null=True)
    City = models.CharField(max_length = 30, verbose_name='Місто', blank=True, null=True)
    Recipient = models.CharField(max_length = 30, verbose_name='Відділення', blank=True, null=True)
    Person = models.CharField(max_length = 30, verbose_name='Контактна особа', blank=True, null=True)
    Phone = models.CharField(max_length = 30, verbose_name='Телефон контактної особи', blank=True, null=True)
    Department = models.TextField(blank=True, null=True, verbose_name='Виконавець')
    DepartmentCode = models.CharField(max_length=9, verbose_name='Код виконавця', blank=True, null=True)
    FormOfPayment = models.CharField(choices=FORMOFPAYMENT, verbose_name='Форма оплати', max_length=2, default="BT",
                                     blank=True, null=True)
    Shipping = models.IntegerField(choices=SHIPPING, default=0, verbose_name='Доставка',blank=True, null=True)

    def __str__( self ):
        return self.Name

class ClientUsers(models.Model):
    Client = models.ForeignKey(Clients, on_delete = models.CASCADE)
    UserId = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    def __str__( self ):
       return str(self.UserId)


class ProfileTypes(models.Model):
    Name = models.CharField(max_length= 100)
    ImageFileName = models.CharField(max_length=100, default='seal2.png')

    def __str__( self ):
        return self.Name

    def local_name( self ):
        return _(self.Name)


class Profiles(models.Model):
    Name = models.CharField(max_length= 100)
    Code1c = models.CharField(max_length= 9)
    NumberOfD = IntegerRangeField(default=1, max_value = 4, min_value = 0)
    NumberOfL = IntegerRangeField(default=1, max_value = 4, min_value = 0)
    ImageFileName = models.CharField(max_length= 100, default = 'seal2.png')
    EditImageFileName = models.CharField(max_length=100, default='seal2.png')
    ProfileType = models.ForeignKey(ProfileTypes, on_delete=models.CASCADE, verbose_name='Тип профіля', blank=True, null=True)
    D1X = IntegerRangeField(default=0, max_value = 1000, min_value = 0)
    D1Y = IntegerRangeField(default=0, max_value = 1000, min_value = 0)
    D2X = IntegerRangeField(default=0, max_value = 1000, min_value = 0)
    D2Y = IntegerRangeField(default=0, max_value = 1000, min_value = 0)
    D3X = IntegerRangeField(default=0, max_value = 1000, min_value = 0)
    D3Y = IntegerRangeField(default=0, max_value = 1000, min_value = 0)
    D4X = IntegerRangeField(default=0, max_value = 1000, min_value = 0)
    D4Y = IntegerRangeField(default=0, max_value = 1000, min_value = 0)
    L1X = IntegerRangeField(default=0, max_value = 1000, min_value = 0)
    L1Y = IntegerRangeField(default=0, max_value = 1000, min_value = 0)
    L2X = IntegerRangeField(default=0, max_value = 1000, min_value = 0)
    L2Y = IntegerRangeField(default=0, max_value = 1000, min_value = 0)
    L3X = IntegerRangeField(default=0, max_value = 1000, min_value = 0)
    L3Y = IntegerRangeField(default=0, max_value = 1000, min_value = 0)
    L4X = IntegerRangeField(default=0, max_value = 1000, min_value = 0)
    L4Y = IntegerRangeField(default=0, max_value = 1000, min_value = 0)
    NumberOfMaterial = IntegerRangeField(default=1, max_value = 4, min_value = 1)
    K1 = IntegerRangeField(default=1, min_value = 0)
    K2 = IntegerRangeField(default=1, min_value = 0)
    K3 = IntegerRangeField(default=1, min_value = 0)
    K4 = IntegerRangeField(default=1, min_value = 0)
    Description = models.TextField(blank=True, null=True, verbose_name='Опис')
    Restriction = models.TextField(blank=True, null=True, verbose_name='Обмеження')
    RestrictionError = models.TextField(blank=True, null=True, verbose_name='Опис обмеження')

    def __str__( self ):
        return self.Name


class Materials(models.Model):
    Name = models.CharField(max_length= 100)
    Code1c = models.CharField(max_length= 9)

    def __str__( self ):
        return self.Name

class Orders(models.Model):
    STATUS = (
        ('calculation', 'Калькуляция'),
        ('order', 'Заказ'),
        ('ready_work', 'Передан в работу'),
        ('in_progress', 'В работе'),
        ('complete', 'Выполнен'),
    )
    FORMOFPAYMENT = (('CP', 'CP'),
                    ('BT', 'BT'))

    SHIPPING = (
        (0, "За замовчуванням"),
        (1, "Відправка клієнту"),
        (5, "Самовивіз"),
    )

    Client = models.ForeignKey(Clients, on_delete=models.CASCADE, verbose_name='Клієнт')
    Number = models.CharField(max_length = 9, verbose_name='Номер', blank=True, null=True)
    Date = models.DateField(blank=True, verbose_name='Дата', null=True)
    Status = models.CharField(choices=STATUS, verbose_name='Статус', max_length=20)
    Summa = models.FloatField(blank=True, null=True, verbose_name='Сума')
    SysDate = models.DateField(auto_now_add=True)
    Loaded = models.BooleanField(default= False)
    BillNumber = models.CharField(max_length = 11, verbose_name='Номер рахунку', blank=True, null=True)
    BillDate = models.DateField(blank=True, verbose_name='Дата рахунку', null=True)
    ApprovalStatus = models.IntegerField(default= 0, verbose_name='Підтвердження')
    IsInvoice = models.BooleanField(default= False)
    FormOfPayment = models.CharField(choices=FORMOFPAYMENT, verbose_name='Форма оплати', max_length=2, default = "BT")
    ConsignmentNote = models.CharField(max_length = 16, verbose_name='Номер ТТН', blank=True, null=True)
    Shipping = models.IntegerField(choices=SHIPPING, default=0, verbose_name='Доставка')
    City = models.CharField(max_length = 30, verbose_name='Місто', blank=True, null=True)
    Recipient = models.CharField(max_length = 30, verbose_name='Відділення', blank=True, null=True)
    Person = models.CharField(max_length = 30, verbose_name='Контактна особа', blank=True, null=True)
    Phone = models.CharField(max_length = 30, verbose_name='Телефон контактної особи', blank=True, null=True)
    Author = models.CharField(max_length=30, verbose_name='Автор', blank=True, null=True)
    Comment = models.TextField(blank=True, null=True, verbose_name='Коментар')
    Department = models.TextField(blank=True, null=True, verbose_name='Виконавець')
    DepartmentCode = models.CharField(max_length=9, verbose_name='Код виконавця', blank=True, null=True)

    def short_number( self ):
        try:
            short_n = str(int(self.Number))
        except:
            short_n = self.Number
        return short_n

    def short_comment( self ):
        try:
            short_c = self.Comment[:10] + "..." if len(self.Comment) > 10 else self.Comment
        except:
            short_c = self.Comment
        return short_c


    def __str__( self ):
        return 'Замовлення № {} від {} (id = {})'.format(self.short_number(), self.Date, self.id) if not self.Number == None else 'Нове замовлення (id = {})'.format(self.id)

    def short_status( self ):
        if self.Status == "calculation":
            return _('Калк.')
        elif self.Status == "order":
            return _('Замов.')
        elif self.Status == "ready_work":
            return _('Перед.')
        elif self.Status == "in_progress":
            return _('В роб.')
        elif self.Status == "complete":
            return _('Викон.')

        return self.Status

    def full_status( self ):
        if self.Status == "calculation":
            return _('calculation')
        elif self.Status == "order":
            return _('order')
        elif self.Status == "ready_work":
            return _('ready_work')
        elif self.Status == "in_progress":
            return _('in_progress')
        elif self.Status == "complete":
            return _('complete')

        return self.Status


    def approval_status_text( self ):
        if self.ApprovalStatus == 1:
            return _('approved')
        elif self.ApprovalStatus == -1:
            return _('rejected')
        else:
            return ""

class OrderLines(models.Model):
    Order = models.ForeignKey(Orders, verbose_name='Замовлення', on_delete=models.CASCADE)
    Quantity = models.FloatField(null=True, default = 1, verbose_name = 'Кількість')
    Profile = models.ForeignKey(Profiles, verbose_name = 'Тип профіля', on_delete=models.CASCADE)
    D1 = models.FloatField(default=0)
    D2 = models.FloatField(default=0)
    D3 = models.FloatField(default=0)
    D4 = models.FloatField(default=0)
    L1 = models.FloatField(default=0)
    L2 = models.FloatField(default=0)
    L3 = models.FloatField(default=0)
    L4 = models.FloatField(default=0)

    Material1 = models.ForeignKey(Materials, verbose_name='Матеріал 1-ї компоненти', on_delete=models.CASCADE,
                                      null=True)
    Material2 = models.ForeignKey(Materials, related_name='+', verbose_name='Матеріал 2-ї компоненти', on_delete=models.CASCADE, blank=True,
                                      null=True)
    Material3 = models.ForeignKey(Materials, related_name='+', verbose_name='Матеріал 3-ї компоненти', on_delete=models.CASCADE, blank=True,
                                      null=True)
    Material4 = models.ForeignKey(Materials, related_name='+', verbose_name='Матеріал 4-ї компоненти', on_delete=models.CASCADE, blank=True,
                                      null=True)

    Comment = models.TextField(blank=True, null=True, verbose_name='Коментар')

    K1 = IntegerRangeField(default=1, min_value = 0)
    K2 = IntegerRangeField(default=1, min_value = 0)
    K3 = IntegerRangeField(default=1, min_value = 0)
    K4 = IntegerRangeField(default=1, min_value = 0)

    NonStandard = models.BooleanField(default= False)

class StabdardRows(models.Model):
    Profile = models.ForeignKey(Profiles, verbose_name = 'Тип профіля', on_delete=models.CASCADE)
    D1 = models.FloatField(null=True)
    D2 = models.FloatField(null=True)
    D3 = models.FloatField(null=True)
    D4 = models.FloatField(null=True)
    L1 = models.FloatField(null=True)
    L2 = models.FloatField(null=True)
    L3 = models.FloatField(null=True)
    L4 = models.FloatField(null=True)

    def __str__( self ):
        return '{}: {} x {}'.format(self.Profile, self.L1, self.L2)

class Categories(models.Model):
    Name = models.CharField(max_length= 100)
    Code1c = models.CharField(max_length= 9)
    ImageFileName = models.CharField(max_length=100, default='seal2.png')
    Folder_id = models.IntegerField(blank=True, null=True)


    def __str__( self ):
        return self.Name

    def local_name( self ):
        return _(self.Name)



class Nomenclatures(models.Model):
    Name = models.CharField(max_length= 100)
    Article = models.CharField(max_length= 30)
    Code1c = models.CharField(max_length= 9)
    Category = models.ForeignKey(Categories, on_delete=models.CASCADE, verbose_name='Категорія', blank=True, null=True)
    ImageFileName = models.CharField(max_length=100, default='seal2.png')

    def __str__( self ):
        return self.Name

    def local_name( self ):
        return _(self.Name)


class DeliveryOrders(models.Model):
    STATUS = (
        ('calculation', 'Калькуляция'),
        ('order', 'Заказ'),
        ('ready_work', 'Передан в работу'),
        ('in_progress', 'В работе'),
        ('complete', 'Выполнен'),
    )
    FORMOFPAYMENT = (('CP', 'CP'),
                    ('BT', 'BT'))

    SHIPPING = (
        (0, "За замовчуванням"),
        (1, "Відправка клієнту"),
        (5, "Самовивіз"),
    )

    Client = models.ForeignKey(Clients, on_delete=models.CASCADE, verbose_name='Клієнт')
    Number = models.CharField(max_length = 9, verbose_name='Номер', blank=True, null=True)
    Date = models.DateField(blank=True, verbose_name='Дата', null=True)
    Status = models.CharField(choices=STATUS, verbose_name='Статус', max_length=20)
    Summa = models.FloatField(blank=True, null=True, verbose_name='Сума')
    SysDate = models.DateField(auto_now_add=True)
    Loaded = models.BooleanField(default= False)
    BillNumber = models.CharField(max_length = 11, verbose_name='Номер рахунку', blank=True, null=True)
    BillDate = models.DateField(blank=True, verbose_name='Дата рахунку', null=True)
    ApprovalStatus = models.IntegerField(default= 0, verbose_name='Підтвердження')
    IsInvoice = models.BooleanField(default= False)
    FormOfPayment = models.CharField(choices=FORMOFPAYMENT, verbose_name='Форма оплати', max_length=2, default = "BT")
    ConsignmentNote = models.CharField(max_length = 16, verbose_name='Номер ТТН', blank=True, null=True)
    Shipping = models.IntegerField(choices=SHIPPING, default=0, verbose_name='Доставка')
    City = models.CharField(max_length = 30, verbose_name='Місто', blank=True, null=True)
    Recipient = models.CharField(max_length = 30, verbose_name='Відділення', blank=True, null=True)
    Person = models.CharField(max_length = 30, verbose_name='Контактна особа', blank=True, null=True)
    Phone = models.CharField(max_length = 30, verbose_name='Телефон контактної особи', blank=True, null=True)
    Author = models.CharField(max_length=30, verbose_name='Автор', blank=True, null=True)
    Comment = models.TextField(blank=True, null=True, verbose_name='Коментар')
    Department = models.TextField(blank=True, null=True, verbose_name='Виконавець')
    DepartmentCode = models.CharField(max_length=9, verbose_name='Код виконавця', blank=True, null=True)

    def short_number( self ):
        short_n = str(self.id)
        return short_n

    def short_comment( self ):
        try:
            short_c = self.Comment[:10] + "..." if len(self.Comment) > 10 else self.Comment
        except:
            short_c = self.Comment
        return short_c


    def __str__( self ):
        return 'Замовлення № {} від {} ({})'.format(self.id, self.Date, self.Status)

    def short_status( self ):
        if self.Status == "calculation":
            return _('Калк.')
        elif self.Status == "order":
            return _('Замов.')
        elif self.Status == "ready_work":
            return _('Перед.')
        elif self.Status == "in_progress":
            return _('В роб.')
        elif self.Status == "complete":
            return _('Викон.')

        return self.Status

    def full_status( self ):
        if self.Status == "calculation":
            return _('calculation')
        elif self.Status == "order":
            return _('order')
        elif self.Status == "ready_work":
            return _('ready_work')
        elif self.Status == "in_progress":
            return _('in_progress')
        elif self.Status == "complete":
            return _('complete')

        return self.Status


    def approval_status_text( self ):
        if self.ApprovalStatus == 1:
            return _('approved')
        elif self.ApprovalStatus == -1:
            return _('rejected')
        else:
            return ""

    def ready_to_postpone( self ):
        delivery_lines = DeliveryOrderLines.objects.filter(DeliveryOrder = self)
        if delivery_lines.count() > 0:
            for delivery_line in delivery_lines:
                if delivery_line.Department == "":
                    return False
            return True
        else:
            return False

class DeliveryOrderLines(models.Model):
    DeliveryOrder = models.ForeignKey(DeliveryOrders, verbose_name='Замовлення', on_delete=models.CASCADE)
    Quantity = models.FloatField(null=True, default = 1, verbose_name = 'Кількість')
    Nomenclature = models.CharField(max_length = 100, verbose_name='Номенклатура', blank=True, null=True)
    Article = models.CharField(max_length = 30, verbose_name='Aртикул', blank=True, null=True)
    Price = models.FloatField(blank=True, null=True, verbose_name='Ціна')
    Summa = models.FloatField(blank=True, null=True, verbose_name='Сума')
    Comment = models.TextField(blank=True, null=True, verbose_name='Коментар')
    Department = models.TextField(blank=True, null=True, verbose_name='Виконавець')
    DepartmentCode = models.CharField(max_length=9, verbose_name='Код виконавця', blank=True, null=True)
    Id_HH = models.IntegerField(default=0)


class DeliveryOrdersHH(models.Model):
    STATUS = (
        ('calculation', 'Калькуляция'),
        ('order', 'Заказ'),
        ('ready_work', 'Передан в работу'),
        ('in_progress', 'В работе'),
        ('complete', 'Выполнен'),
    )

    DeliveryOrder = models.ForeignKey(DeliveryOrders, verbose_name='Замовлення', on_delete=models.CASCADE)
    Number = models.CharField(max_length = 9, verbose_name='Номер', blank=True, null=True)
    Date = models.DateField(blank=True, verbose_name='Дата', null=True)
    Status = models.CharField(choices=STATUS, verbose_name='Статус', max_length=20)
    Summa = models.FloatField(blank=True, null=True, verbose_name='Сума')
    BillNumber = models.CharField(max_length = 11, verbose_name='Номер рахунку', blank=True, null=True)
    BillDate = models.DateField(blank=True, verbose_name='Дата рахунку', null=True)
    IsInvoice = models.BooleanField(default= False)
    Department = models.TextField(blank=True, null=True, verbose_name='Виконавець')
    DepartmentCode = models.CharField(max_length=9, verbose_name='Код виконавця', blank=True, null=True)
    ConsignmentNote = models.CharField(max_length = 16, verbose_name='Номер ТТН', blank=True, null=True)

    def short_status( self ):
        if self.Status == "calculation":
            return _('Калк.')
        elif self.Status == "order":
            return _('Замов.')
        elif self.Status == "ready_work":
            return _('Перед.')
        elif self.Status == "in_progress":
            return _('В роб.')
        elif self.Status == "complete":
            return _('Викон.')

        return self.Status

    def full_status( self ):
        if self.Status == "calculation":
            return _('calculation')
        elif self.Status == "order":
            return _('order')
        elif self.Status == "ready_work":
            return _('ready_work')
        elif self.Status == "in_progress":
            return _('in_progress')
        elif self.Status == "complete":
            return _('complete')

        return self.Status

    def short_number( self ):
        try:
            short_n = str(int(self.Number))
        except:
            short_n = self.Number
        return short_n
