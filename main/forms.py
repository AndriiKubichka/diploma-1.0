from django import forms
from .models import Orders
from .models import OrderLines
from .models import DeliveryOrders
from .models import DeliveryOrderLines

from django.forms import ModelForm, Textarea, HiddenInput, NumberInput


class OrderForm(forms.ModelForm):
    class Meta:
        model = Orders
        exclude = ['Number', 'Date', 'Status', 'Client']
        #fields = '__all__'

class OrderLineForm(forms.ModelForm):
    def __init__ ( self, *args, **kwargs ) :
       super().__init__(*args, **kwargs)

       #self.fields ['Order'].disabled = True


    class Meta:
        model = OrderLines
        fields = '__all__'
        widgets = {
            'Comment' : Textarea(attrs={'cols' : 80, 'rows' : 5}),
            'Order': HiddenInput(),
            'Quantity':  NumberInput(attrs = {'autofocus': 'autofocus'}),
            'K1': NumberInput(attrs={'style': 'width: 50px'}),
            'K2' : NumberInput(attrs={'style' : 'width: 50px'}),
            'K3' : NumberInput(attrs={'style' : 'width: 50px'}),
            'K4' : NumberInput(attrs={'style' : 'width: 50px'})
            }

class DeliveryOrderForm(forms.ModelForm):
    class Meta:
        model = DeliveryOrders
        exclude = ['Number', 'Date', 'Status', 'Client']
        #fields = '__all__'

class DeliveryOrderLineForm(forms.ModelForm):
    def __init__ ( self, *args, **kwargs ) :
       super().__init__(*args, **kwargs)

       #self.fields ['Order'].disabled = True


    class Meta:
        model = DeliveryOrderLines
        fields = '__all__'
        widgets = {
            'Comment' : Textarea(attrs={'cols' : 80, 'rows' : 5}),
            'Order': HiddenInput(),
            'Quantity':  NumberInput(attrs = {'autofocus': 'autofocus'}),
            }


