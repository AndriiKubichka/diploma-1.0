from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from .models import Orders, ClientUsers, OrderLines, Profiles, ProfileTypes, Materials, StabdardRows
from .models import DeliveryOrders, DeliveryOrderLines, Nomenclatures, Categories, DeliveryOrdersHH
from .forms import OrderLineForm, OrderForm, DeliveryOrderForm
import httplib2
from suds import client as suds_client
from suds import transport as suds_transport
from suds.plugin import MessagePlugin
from suds.sax import element as suds_element
from .my_settings import *
import json
from django.utils.translation import LANGUAGE_SESSION_KEY
import io as BytesIO
import base64
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django_user_agents.utils import get_user_agent
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth import update_session_auth_hash
from django.utils.translation import ugettext
from django.forms.models import model_to_dict
from datetime import date
import ast
from base64 import b64encode
from django import template



# Create your views here.

def home(request):
    return render(request,'home_page.html', {'current_language': request.session.get(LANGUAGE_SESSION_KEY)})


def new_order(request):

    if not request.user.is_authenticated:
        return redirect('/')

    client = ClientUsers.objects.get(UserId_id = request.user.id).Client
    clientWS = suds_client.Client(PATH_2_SERVIСES, username = WEB_USER, password = WEB_USER_PASSWORD)
    try:
        result = clientWS.service.GetListOfSealDepartment(client.EDRPOU, client.Password)
        departments_list0 = json.loads(str(result))
        departments_list = departments_list0 [ 'Departments' ]
    except:
        pass

    default_data = {
        'Client' : client,
        'Status' : 'calculation'
    }

    if client.Shipping == 1:
        default_data.update(Shipping = client.Shipping)
        default_data.update(City = client.City)
        default_data.update(Person = client.Person)
        default_data.update(Phone = client.Phone)
        default_data.update(Recipient = client.Recipient)

    elif client.Shipping == 5:
        default_data.update(Shipping = client.Shipping)

    new_order = Orders.objects.create(**default_data)

    author = " ".join([request.user.first_name, request.user.last_name])
    author = author[:29]

    Orders.objects.filter(id=new_order.id).update(Author = author, Department=client.Department, DepartmentCode=client.DepartmentCode)

    if client.FormOfPayment == "CP":
        Orders.objects.filter(id=new_order.id).update(FormOfPayment="CP")

    request.session['order_id'] = new_order.id
    lines = OrderLines.objects.filter(Order_id=new_order.id)

    context = {
        'action': '/edit_order/',
        'order': new_order,
        'lines': lines,
        'order_srt': str(new_order),
        'current_language': request.session.get(LANGUAGE_SESSION_KEY),
        'departments_list': departments_list,
        'form_of_payment': "CP" if client.FormOfPayment == "CP" else "BT"
    }

    return render(request, 'edit_order.html', context)


def new_line(request):

    if not request.user.is_authenticated:
        return redirect('/')

    context = {
        'current_language' : request.session.get(LANGUAGE_SESSION_KEY),
    }
    get_order_id = request.session.get('order_id')

    order = Orders.objects.get(id=get_order_id)
    lines = OrderLines.objects.filter(Order_id = order.id)

    if request.method == 'GET':
        context.update(form = OrderLineForm(initial = {'Order': order}))
        context.update(action = '/new_line/')

    elif request.method == 'POST':
        form = OrderLineForm(request.POST, initial = {'Order': order})
        if form.is_valid():

            client = ClientUsers.objects.get(UserId_id=request.user.id).Client
            clientWS = suds_client.Client(PATH_2_SERVIСES, username=WEB_USER, password=WEB_USER_PASSWORD)
            try :
                result = clientWS.service.GetListOfSealDepartment(client.EDRPOU, client.Password)
                departments_list0 = json.loads(str(result))
                departments_list = departments_list0 [ 'Departments' ]
            except :
                departments_list = [{"DepartmentName" : order.Department, "DepartmentCode" : order.DepartmentCode }]

            form.save()

            context.update(action='/edit_order/')
            context.update(order = order)
            context.update(lines = lines)
            context.update(order_str = str(order))
            context.update(departments_list=departments_list)

            request.session['order_id'] = order.id

            return render(request, 'edit_order.html', context)
        else:
            #form.errors
            context.update(form = form)

    return render(request, 'new_line.html', context)

def list_order(request, errormessage = None, successmessage = None):
    import datetime
    context = {
        'current_language' : request.session.get(LANGUAGE_SESSION_KEY),
        'is_mobile': get_user_agent(request).is_mobile
    }

    #print("request.headers.user_agent.is_mobile = ", get_user_agent(request).is_mobile)
    #print("request.headers.user_agent.is_tablet = ", get_user_agent(request).is_tablet)
    #print("request.headers.user_agent.is_touch_capable = ", get_user_agent(request).is_touch_capable)
    #print("request.headers.user_agent.is_pc = ", get_user_agent(request).is_pc)
    #print("request.headers.user_agent.is_bot = ", get_user_agent(request).is_bot)

    list_filter = request.GET.get('filter')

    client_org = ClientUsers.objects.get(UserId_id=request.user.id).Client

    if 'search' in request.GET :
        search_str = request.GET [ 'search' ]
        orders = Orders.objects.filter(Client=client_org, Loaded=True).filter(Comment__icontains=search_str).exclude(IsInvoice=True).exclude(ApprovalStatus=-1)
    else:
        search_str = ''
        orders = Orders.objects.filter(Client=client_org, Loaded=True).exclude(IsInvoice=True).exclude(ApprovalStatus=-1)

    req_orders = []
    for order in orders:
        req_order = {
            'OrderNum': str(order.id),
            'OrderDate' : order.SysDate.strftime("%Y%m%d"),
        }
        req_orders.append(req_order)

    client = suds_client.Client(PATH_2_SERVIСES, username = WEB_USER, password = WEB_USER_PASSWORD)

    try:
        result = client.service.UpdateListOrder(client_org.EDRPOU, client_org.Password, str(req_orders))

        answer_dict0 = json.loads(str(result))
        answer_list = answer_dict0 [ 'Strings' ]

        for elem in answer_list :
            status_str = elem [ 'Status' ]

            if status_str == 'Калькуляция' :
                status = 'calculation'
            elif status_str == 'Заказ' :
                status = 'order'
            elif status_str == "Передано в производство" :
                status = 'ready_work'
            elif status_str == 'В работе' :
                status = 'in_progress'
            elif status_str == 'Выполнено' :
                status = 'complete'

            try :
                order_date = datetime.datetime(int(elem [ 'OrderDate' ] [ 0 :4 ]), int(elem [ 'OrderDate' ] [ 4 :6 ]),
                                       int(elem [ 'OrderDate' ] [ 6 :8 ]))
            except :
                order_date = None

            try :
                bill_date = datetime.datetime(int(elem [ 'BillDate' ] [ 0 :4 ]), int(elem [ 'BillDate' ] [ 4 :6 ]),
                                      int(elem [ 'BillDate' ] [ 6 :8 ]))
            except :
                bill_date = None

            is_invoice = False
            try :
                is_invoice = not (elem [ 'InvoiceNum' ] == '')
            except :
                pass

            sum_doc = 0
            try:
                for bills_line in elem [ "BillStrings" ] :
                    if elem [ 'BillIncludesVAT' ] == '0' :
                        sum_doc += round(
                        float(bills_line [ "Quantity" ]) * float(bills_line [ "Price" ]) * (1 + VAT_RATE / 100), 2)
            except:
                pass

            try:
                bill_num = elem ['BillNum']
            except:
                bill_num = None

            order_id = int(elem["Id"])
            DeclarationNum = elem['DeclarationNum']

            Orders.objects.filter(id=order_id).update(Number=elem [ 'OrderNum' ], Date=order_date, Status=status,
                                                  Summa=sum_doc, BillNumber=bill_num, BillDate=bill_date,
                                                  IsInvoice=is_invoice, ConsignmentNote=DeclarationNum)
    except:
        errormessage = ugettext("The Hydrohouse base is not available. Please try again later")
        pass

    if search_str == '' :
        if list_filter == 'New':
            orders = Orders.objects.filter(Client = client_org, Loaded=False).order_by('-id')
        elif list_filter == 'Loaded':
            orders = Orders.objects.filter(Client = client_org, Loaded=True, Status='calculation').order_by('-id')
        elif list_filter == 'ToApprove':
            orders = Orders.objects.filter(Client = client_org, ApprovalStatus = 0).order_by('-id').exclude(Status='calculation')
        elif list_filter == 'InWork':
            orders = Orders.objects.filter(Client = client_org, ApprovalStatus = 1).order_by('-id').exclude(Status='complete').exclude(Status='calculation')
        elif list_filter == 'Complete':
            orders = Orders.objects.filter(Client = client_org, Status='complete').order_by('-id')
        else:
            orders = Orders.objects.filter(Client=client_org).order_by('-id')
            list_filter = 'All'
    else:
        orders = Orders.objects.filter(Client=client_org).filter(Comment__icontains=search_str).order_by('-id')
        list_filter = 'All'

    context.update(filter=list_filter)
    context.update(search_str=search_str)

    paginator = Paginator(orders, 10)
    page = request.GET.get('page')

    try :
        orders = paginator.page(page)
    except PageNotAnInteger :
        # If page is not an integer, deliver first page.
        orders = paginator.page(1)
        orders = paginator.page(1)
    except EmptyPage :
        # If page is out of range (e.g. 9999), deliver last page of results.
        orders = paginator.page(paginator.num_pages)

    context.update(orders=orders)
    if errormessage:
        context.update(errormessage=errormessage)
    if successmessage:
        context.update(successmessage=successmessage)

    return render(request, 'list_order.html', context)

#    if request.method == 'GET':
#        context.update(orders = orders)
#        return render(request, 'list_order.html', context)

def ajax_delete_order(request):
    if request.is_ajax():
        order_id = int(request.GET.get('id'))
        Orders.objects.get(id=order_id).delete()
        return JsonResponse({
            'status': True
        })

def edit_order(request, order_id):

    if not request.user.is_authenticated:
        return redirect('/')

    order = Orders.objects.get(id = order_id)
    lines = OrderLines.objects.filter(Order_id=order.id)

    client = order.Client

    clientWS = suds_client.Client(PATH_2_SERVIСES, username = WEB_USER, password = WEB_USER_PASSWORD)

    try:
        result = clientWS.service.GetListOfSealDepartment(client.EDRPOU, client.Password)
        departments_list0 = json.loads(str(result))
        departments_list = departments_list0 [ 'Departments' ]

    except:
        departments_list = [ {"DepartmentName" : order.Department, "DepartmentCode" : order.DepartmentCode} ]

    context = {
        'action': '/edit_order/',
        'order' : order,
        'lines' : lines,
        'order_srt' : str(order),
        'current_language' : request.session.get(LANGUAGE_SESSION_KEY),
        'departments_list' : departments_list,
    }
    request.session['order_id'] = order.id
    return render(request, 'edit_order.html', context)

def edit_line(request, line_id):

    if not request.user.is_authenticated:
        return redirect('/')

    line = OrderLines.objects.get(id = line_id)
    standard_rows = StabdardRows.objects.filter(Profile=line.Profile)

    init_dict = {'Order' : line.Order,
                 'Profile' : line.Profile,
                 'D1': line.D1,
                 'D2' : line.D2,
                 'D3' : line.D3,
                 'D4' : line.D4,
                 'L1' : line.L1,
                 'L2' : line.L2,
                 'L3' : line.L3,
                 'L4' : line.L4,
                 'Quantity': line.Quantity,
                 'Material1': line.Material1,
                 'Material2' : line.Material2,
                 'Material3' : line.Material3,
                 'Material4' : line.Material4,
                 'K1' : line.K1,
                 'K2' : line.K2,
                 'K3' : line.K3,
                 'K4' : line.K4,
                 'Comment' : line.Comment,
                 'NonStandard': line.NonStandard,
                 'standard_rows': standard_rows,
                 }

    #form = OrderLineForm(initial=init_dict)

    context = {
        'imagefile' : line.Profile.EditImageFileName,
        'profile_txt' : str(line.Profile),
        'profile' : line.Profile,
        'form' : OrderLineForm(init_dict),
        'current_language' : request.session.get(LANGUAGE_SESSION_KEY),
        'order' : line.Order,
        'd1' : str(line.D1).replace(',','.'),
        'd2' : str(line.D2).replace(',','.'),
        'd3' : str(line.D3).replace(',','.'),
        'd4' : str(line.D4).replace(',','.'),
        'l1' : str(line.L1).replace(',','.'),
        'l2' : str(line.L2).replace(',','.'),
        'l3' : str(line.L3).replace(',','.'),
        'l4' : str(line.L4).replace(',','.'),
        'line_id' : line.id,
        'Description' : line.Profile.Description,
        'NonStandard' : line.NonStandard,
        'standard_rows' : standard_rows,
    }
    #if form.is_valid() :

    return render(request, 'new_seal.html', context)

    #return list_order(request)

def ajax_delete_line(request):
    if request.is_ajax():
        line_id = int(request.GET.get('id'))
        OrderLines.objects.get(id = line_id).delete()
        return JsonResponse({
            'status': True
        })

def send_new_order(request, order_id) :
    import datetime

    if not request.user.is_authenticated:
        return redirect('/')

    order = Orders.objects.get(id=order_id)
    lines = OrderLines.objects.filter(Order_id=order.id)

    errormessage = None
    successmessage = None

    try:
        client = suds_client.Client(PATH_2_SERVIСES, username = WEB_USER, password = WEB_USER_PASSWORD)

        number = 0
        stings = []
        for line in lines:
            number = number + 1
            K2 = ""
            if line.Material2:
                if line.Profile.K2 > 0:
                    K2 = str(line.Profile.K2)
                else:
                    K2 = str(line.K2)

            K3 = ""
            if line.Material3:
                if line.Profile.K3 > 0:
                    K3 = str(line.Profile.K3)
                else:
                    K3 = str(line.K3)

            K4 = ""
            if line.Material4:
                if line.Profile.K4 > 0:
                    K4 = str(line.Profile.K4)
                else:
                    K4 = str(line.K4)

            stings.append(
                {
                    "Row" : str(number),
                    "RowId" : str(line.id % 100000),
                    "Profile" : line.Profile.Name,
                    "D1" : str(line.D1),
                    "D2" : str(line.D2),
                    "D3" : str(line.D3),
                    "D4" : str(line.D4),
                    "H1" : str(line.L1),
                    "H2" : str(line.L2),
                    "H3" : str(line.L3),
                    "H4" : str(line.L4),
                    "Quantity" : str(line.Quantity),
                    "Material" : line.Material1.Name,
                    "Comment" : line.Comment,
                    "MaterialCode1": line.Material1.Code1c if line.Material1 else "",
                    "MaterialCode2": line.Material2.Code1c if line.Material2 else "",
                    "MaterialCode3": line.Material3.Code1c if line.Material3 else "",
                    "MaterialCode4" : line.Material4.Code1c if line.Material4 else "",
                    "K1": str(line.K1),
                    "K2" : K2,
                    "K3" : K3,
                    "K4" : K4,
                    "NonStandard": "1" if line.NonStandard else "",
                }

            )

        req_test = {
            "EDRPOU" : order.Client.EDRPOU,
            "OrderNum" : str(order_id),
            "UserName" : request.user.first_name,
            "OrderDate" : order.SysDate.strftime("%Y%m%d"),
            "CP" : "1" if order.FormOfPayment == "CP" else "0",
            "Strings" : stings,
            "Comment": "" if order.Comment == None else order.Comment
        }

        if order.DepartmentCode:
            req_test.update(Department = order.DepartmentCode)

        if order.Shipping == 5:
            req_test.update(Shipping = "5")
        if order.Shipping == 1:
            req_test.update(Shipping = "1")
            req_test.update(City = order.City)
            req_test.update(Recipient = order.Recipient)
            req_test.update(Person = order.Person)
            req_test.update(Phone = order.Phone)

        result = client.service.PostSealOrder(order.Client.EDRPOU, order.Client.Password, str(req_test))

        answer_dict0 = json.loads(str(result))
        answer_list = answer_dict0['Strings']

        for elem in answer_list:
            if elem['Comment'] == "":
                if elem['Return'] == "The order was successfully added":
                    Orders.objects.filter(id=order_id).update(Loaded = True)
                    successmessage = ugettext('The order was successfully added')
                else:
                    errormessage = ugettext('An error occurred while sending order ') + str(order_id) + ' :' + ugettext(elem [ 'Return' ])
            else:
                if elem [ 'Return' ] == "The order was successfully added" :
                    Orders.objects.filter(id=order_id).update(Loaded=True)
                    successmessage = ugettext('The order was successfully added')
                    errormessage = ugettext('The but message') + elem['Comment'] + ugettext('Recomendation if not ordered')
                else :
                    errormessage = ugettext('An error occurred while sending order ') + str(
                           order_id) + ' :' + ugettext(elem [ 'Return' ])


    except:
        errormessage = ugettext('An error occurred while sending order :') + str(order_id)
        order = Orders.objects.get(id=order_id)

        client = order.Client

        clientWS = suds_client.Client(PATH_2_SERVIСES, username=WEB_USER, password=WEB_USER_PASSWORD)

        try :
            result = clientWS.service.GetListOfSealDepartment(client.EDRPOU, client.Password)
            departments_list0 = json.loads(str(result))
            departments_list = departments_list0 [ 'Departments' ]

        except :
            departments_list = [ {"DepartmentName" : order.Department, "DepartmentCode" : order.DepartmentCode} ]

        context = {
            'current_language' : request.session.get(LANGUAGE_SESSION_KEY),
            'action': '/edit_order/',
            'order': order,
            'lines': lines,
            'errormsg': errormessage,
            'departments_list': departments_list,
        }
        request.session [ 'order_id' ] = order.id
        return render(request, 'edit_order.html', context)

    try:
        client = suds_client.Client(PATH_2_SERVIСES, username = WEB_USER, password = WEB_USER_PASSWORD)

        req_test = {
            "EDRPOU" : order.Client.EDRPOU,
            "OrderNum" : str(order_id),
            "UserName" : request.user.first_name,
            "OrderDate" : order.SysDate.strftime("%Y%m%d"),
        }

        result = client.service.UpdateOrderStatus(order.Client.EDRPOU, order.Client.Password, str(req_test))

        answer_dict0 = json.loads(str(result))
        answer_list = answer_dict0['Strings']

        for elem in answer_list:
            status_str = elem['Status']

            if status_str == 'Калькуляция': status = 'calculation'
            elif status_str == 'Заказ': status = 'order'
            elif status_str == "Передано в производство": status = 'ready_work'
            elif status_str == 'В работе': status = 'in_progress'
            elif status_str == 'Выполнено': status = 'complete'

            order_date = datetime.datetime(int(elem['OrderDate'][ 0 :4 ]), int(elem [ 'OrderDate' ] [ 4 :6 ]), int(elem[ 'OrderDate' ] [ 6 :8 ]))
            bill_date = datetime.datetime(int(elem['BillDate'][ 0 :4 ]), int(elem [ 'BillDate' ] [ 4 :6 ]), int(elem[ 'BillDate' ] [ 6 :8 ]))

            is_invoice = False
            try:
                is_invoice = not (elem['InvoiceNum'] == '')
            except:
                pass

            sum_doc = 0
            for bills_line in elem["BillStrings"]:
                if elem['BillIncludesVAT'] == '0':
                    sum_doc += round(float(bills_line["Quantity"]) * float(bills_line["Price"]) * (1 + VAT_RATE / 100), 2)

            Orders.objects.filter(id = order_id).update(Number = elem['OrderNum'], Date = order_date, Status = status, Summa = sum_doc, BillNumber = elem['BillNum'], BillDate = bill_date, IsInvoice = is_invoice)

    except:
        pass

    return list_order(request, errormessage = errormessage, successmessage = successmessage)


def check_order(request, order_id) :
    import datetime

    if not request.user.is_authenticated:
        return redirect('/')

    type_o = request.GET.get('type_o')
    if type_o == "delivery":
        delivery_order = DeliveryOrders.objects.get(id=order_id)

        try:
            client = suds_client.Client(PATH_2_SERVIСES, username = WEB_USER, password = WEB_USER_PASSWORD)

            req_test = {
                "EDRPOU" : delivery_order.Client.EDRPOU,
                "OrderNum" : str(order_id),
                "UserName" : request.user.first_name,
                "OrderDate" : delivery_order.SysDate.strftime("%Y%m%d"),
            }

            result = client.service.UpdateOrderStatus(delivery_order.Client.EDRPOU, delivery_order.Client.Password, str(req_test))

            answer_dict0 = json.loads(str(result))
            answer_list = answer_dict0['Strings']

            ordersHH = DeliveryOrdersHH.objects.filter(DeliveryOrder_id=order_id)

            if ordersHH.count() > 0 :
                for orderHH in ordersHH :
                    orderHH.delete()

            all_invoices = None
            min_status = None

            for elem_order in answer_list :

                sum_doc = round(float(elem_order [ "Summa" ]), 2)

                status_str = elem_order [ 'Status' ]

                if status_str == 'Калькуляция' :
                    status = 'calculation'
                elif status_str == 'Заказ' :
                    status = 'order'
                elif status_str == "Передано в производство" :
                    status = 'ready_work'
                elif status_str == 'В работе' :
                    status = 'in_progress'
                elif status_str == 'Выполнено' :
                    status = 'complete'

                if min_status is None :
                    min_status = status

                if status == 'calculation' :
                    min_status = status
                if status == 'order' and not min_status == 'order' and not min_status == 'calculation' :
                    min_status = status
                if status == 'ready_work' and (min_status == 'in_progress' or min_status == 'complete') :
                    min_status = status
                if status == 'in_progress' and min_status == 'complete' :
                    min_status = status

                try :
                    bill_date = datetime.datetime(int(elem_order [ 'BillDate' ] [ 0 :4 ]),
                                                      int(elem_order [ 'BillDate' ] [ 4 :6 ]),
                                                      int(elem_order [ 'BillDate' ] [ 6 :8 ]))
                except :
                    bill_date = None

                is_invoice = False
                try :
                    is_invoice = not (elem_order [ 'InvoiceNum' ] == '')
                except :
                    pass

                if all_invoices is None :
                    all_invoices = is_invoice
                else :
                    all_invoices = (all_invoices and is_invoice)

                try :
                    bill_num = elem_order [ 'BillNum' ]
                except :
                    bill_num = None

                DeclarationNum = elem_order [ 'DeclarationNum' ]
                Department = elem_order [ 'Store' ]

                try :
                    DeliveryOrdersHH.objects.create(DeliveryOrder=delivery_order, Date=delivery_order.Date,
                                                    Status=status, Number=elem_order [ 'OrderNum' ],
                                                    Summa=sum_doc, BillNumber=bill_num, BillDate=bill_date,
                                                    IsInvoice=is_invoice, ConsignmentNote=DeclarationNum,
                                                    Department=Department)
                except :
                    pass

            if not all_invoices is None :
                if all_invoices :
                    delivery_order.IsInvoice = True
                    delivery_order.save()

            if not min_status is None :
                delivery_order.Status = min_status
                delivery_order.save()

        except:
            pass

        return list_delivery_order(request)

    elif type_o == "seal":
        order = Orders.objects.get(id=order_id)
        try:
            client = suds_client.Client(PATH_2_SERVIСES, username = WEB_USER, password = WEB_USER_PASSWORD)

            req_test = {
                "EDRPOU" : order.Client.EDRPOU,
                "OrderNum" : str(order_id),
                "UserName" : request.user.first_name,
                "OrderDate" : order.SysDate.strftime("%Y%m%d"),
            }

            result = client.service.UpdateOrderStatus(order.Client.EDRPOU, order.Client.Password, str(req_test))

            answer_dict0 = json.loads(str(result))
            answer_list = answer_dict0['Strings']

            for elem in answer_list:
                status_str = elem['Status']

                if status_str == 'Калькуляция': status = 'calculation'
                elif status_str == 'Заказ': status = 'order'
                elif status_str == "Передано в производство": status = 'ready_work'
                elif status_str == 'В работе': status = 'in_progress'
                elif status_str == 'Выполнено': status = 'complete'

                order_date = datetime.datetime(int(elem['OrderDate'][ 0 :4 ]), int(elem [ 'OrderDate' ] [ 4 :6 ]), int(elem[ 'OrderDate' ] [ 6 :8 ]))
                bill_date = datetime.datetime(int(elem['BillDate'][ 0 :4 ]), int(elem [ 'BillDate' ] [ 4 :6 ]), int(elem[ 'BillDate' ] [ 6 :8 ]))

                is_invoice = False
                try:
                    is_invoice = not (elem['InvoiceNum'] == '')
                except:
                    pass

                DeclarationNum = elem['DeclarationNum']

                sum_doc = 0
                for bills_line in elem["BillStrings"]:
                    if elem['BillIncludesVAT'] == '0':
                        sum_doc += round(float(bills_line["Quantity"]) * float(bills_line["Price"]) * (1 + VAT_RATE / 100), 2)

                sum_doc = format(sum_doc, '.2f')

                Orders.objects.filter(id = order_id).update(Number = elem['OrderNum'], Date = order_date, Status = status, Summa = sum_doc, BillNumber = elem['BillNum'], BillDate = bill_date, IsInvoice = is_invoice, ConsignmentNote = DeclarationNum)

        except:
            pass

        return list_order(request)


def confirm_order(request, order_id) :
    if not request.user.is_authenticated:
        return redirect('/')

    order = Orders.objects.get(id=order_id)
    errormessage = None

    try:
        client = suds_client.Client(PATH_2_SERVIСES, username = WEB_USER, password = WEB_USER_PASSWORD)

        req_test = {
            "EDRPOU" : order.Client.EDRPOU,
            "OrderNum" : order.BillNumber,
            "UserName" : request.user.first_name,
            "OrderDate" : order.BillDate.strftime("%Y%m%d"),
            "Message" : "Confirm",
            "Delivery" : "0",
            "PartnerOrderNum" : order.Number,
            "PartnerOrderDate" : order.Date.strftime("%Y%m%d"),
        }

        result = client.service.ConfirmSealOrder(order.Client.EDRPOU, order.Client.Password, str(req_test))

        if str(result) == "The order is successfully confirmed":
            Orders.objects.filter(id=order_id).update(ApprovalStatus = 1)
        else:
            errormessage = ugettext(str(result))
    except Exception as e:
        errormessage = str(e)
        pass

    #return HttpResponse("CONFIRM ORDER"  + str(result))
    return list_order(request, errormessage = errormessage)

def reject_order(request, order_id) :
    if not request.user.is_authenticated:
        return redirect('/')

    order = Orders.objects.get(id=order_id)

    try:
        client = suds_client.Client(PATH_2_SERVIСES, username = WEB_USER, password = WEB_USER_PASSWORD)

        req_text = {
            "EDRPOU" : order.Client.EDRPOU,
            "OrderNum" : order.BillNumber,
            "UserName" : request.user.first_name,
            "OrderDate" : order.BillDate.strftime("%Y%m%d"),
            "Message" : "Reject",
            "Delivery" : "0",
            "PartnerOrderNum" : order.Number,
            "PartnerOrderDate" : order.Date.strftime("%Y%m%d"),
        }

        result = client.service.ConfirmSealOrder(order.Client.EDRPOU, order.Client.Password, str(req_text))

        if str(result) == "The order is successfully rejected":
            Orders.objects.filter(id=order_id).update(ApprovalStatus = -1)
    except:
        pass

    return list_order(request)

def set_ukr(request):
    request.session [LANGUAGE_SESSION_KEY ] = 'uk'
    next_url = request.GET.get('next', request.GET.get('next'))
    return redirect(next_url)

def set_rus(request):
    request.session [LANGUAGE_SESSION_KEY ] = 'ru'
    next_url = request.GET.get('next', request.GET.get('next'))
    return redirect(next_url)

def get_bill(request, order_id) :
    if not request.user.is_authenticated:
        return redirect('/')

    order = Orders.objects.get(id=order_id)

    try:
        client = suds_client.Client(PATH_2_SERVIСES, username = WEB_USER, password = WEB_USER_PASSWORD)

        req_text = {
            "EDRPOU" : order.Client.EDRPOU,
            "BillNum" : order.BillNumber,
            "UserName" : request.user.first_name,
            "BillDate" : order.BillDate.strftime("%Y%m%d"),
        }

        result = client.service.GetBill(order.Client.EDRPOU, order.Client.Password, str(req_text))
        answer_dict0 = json.loads(str(result))
        answer = answer_dict0['Data']

        FileType = answer_dict0['FileType']

        buffer = BytesIO.BytesIO()
        content = base64.b64decode(answer)
        buffer.write(content)

        if FileType.upper() == 'PDF' :
            response = HttpResponse(
                buffer.getvalue(),
                content_type="application/pdf",
            )
            response [ 'Content-Disposition' ] = 'inline;filename=some_file.pdf'

        if FileType.upper() in ('XLS', 'XLS95', 'XLS97', 'XLSX'):
            response = HttpResponse(
                buffer.getvalue(),
                content_type="application/vnd.ms-excel",
            )
            response [ 'Content-Disposition' ] = 'inline;filename=some_file.xls'

            #response = HttpResponse(
            #buffer.getvalue(),
            #content_type="application/pdf",
            #content_type="application/vnd.ms-excel",
            #)
            #response [ 'Content-Disposition' ] = 'inline;filename=some_file.pdf'
            #response [ 'Content-Disposition' ] = 'inline;filename=some_file.xls'

        return response

    except Exception as e:
        return HttpResponse("Get bill:" + str(e))

    #return HttpResponse("CHECK ORDER " + str(result))
    return list_order(request)

def change_password(request):
    if not request.user.is_authenticated:
        return redirect('/')
    if request.method == 'POST' :
        form = PasswordChangeForm(user=request.user, data=request.POST)
        if form.is_valid() :
            form.save()
            update_session_auth_hash(request, form.user)
            return redirect('/')
        else:
            return render(request, 'change_password.html',
                   {'form' : form,
                    'user' : request.user,
                    'current_language' : request.session.get(LANGUAGE_SESSION_KEY),
                    })
    else:
        form = PasswordChangeForm(request.user)

    return render(request, 'change_password.html',
                  {'form' : form,
                   'user' : request.user,
                   'current_language' : request.session.get(LANGUAGE_SESSION_KEY),
                  })
'''
def new_seal(request):

    get_order_id = request.session.get('order_id')
    order = Orders.objects.get(id=get_order_id)

    if request.method == 'GET' :
        context = {
           'imagefile': "",
           'form': OrderLineForm(initial={'Order' : order}),
           'current_language' : request.session.get(LANGUAGE_SESSION_KEY),
                       }
    else:
        context = {}

    if request.method == 'POST':

        profile_id = request.POST.get('Profile')
        profile_obj = Profiles.objects.get(id = profile_id)
        imagefile = profile_obj.ImageFileName
        profile_txt = str(profile_obj)

        print('profile_txt=', profile_txt)
        print('type(profile_txt)=', type(profile_txt))

        context = {
            'imagefile' : imagefile,
            'profile_txt': profile_txt,
            'form' : OrderLineForm(initial={'Order' : order, 'Profile': profile_obj}),
            'current_language' : request.session.get(LANGUAGE_SESSION_KEY),
        }
        request.session [ 'profile_id' ] = profile_id

    return render(request, 'new_seal.html',
                  context)
'''
def new_seal(request, profile_id):
    get_order_id = request.session.get('order_id')
    order = Orders.objects.get(id=get_order_id)
    profile = Profiles.objects.get(id=profile_id)

    if request.method == 'GET' :
        imagefile = profile.EditImageFileName
        profile_txt = str(profile)

        standard_rows = StabdardRows.objects.filter(Profile = profile)

        context = {
            'imagefile' : imagefile,
            'profile_txt' : profile_txt,
            'profile' : profile,
            'form' : OrderLineForm(initial={'Order' : order, 'Profile' : profile}),
            'current_language' : request.session.get(LANGUAGE_SESSION_KEY),
            'order': order,
            'Description' : profile.Description,
            'standard_rows': standard_rows,
        }
        request.session [ 'profile_id' ] = profile_id

    return render(request, 'new_seal.html',
                  context)

def check_condition(d1,d2,d3,d4,l1,l2,l3,l4,restriction):

    if not restriction:
        return True

    try:
        d1 = float(d1)
    except:
        d1 = 0
    try:
        d2 = float(d2)
    except:
        d2 = 0
    try:
        d3 = float(d3)
    except:
        d3 = 0
    try:
        d4 = float(d4)
    except:
        d4 = 0
    try:
        l1 = float(l1)
    except:
        l1 = 0
    try:
        l2 = float(l2)
    except:
        l2 = 0
    try:
        l3 = float(l3)
    except:
        l3 = 0
    try:
        l4 = float(l4)
    except:
        l4 = 0

    try:
        return eval(restriction)
    except:
        False

def confirm_new_seal(request):
    context = {
        'current_language' : request.session.get(LANGUAGE_SESSION_KEY),
    }

    get_order_id = request.session.get('order_id')
    order = Orders.objects.get(id=get_order_id)
    lines = OrderLines.objects.filter(Order_id = order.id)

    profile_id = request.session.get('profile_id')
    profile = Profiles.objects.get(id = profile_id)
    profile_txt = str(profile)

    if request.method == 'POST':
        d1 = request.POST.get('d1')
        d2 = request.POST.get('d2')
        d3 = request.POST.get('d3')
        d4 = request.POST.get('d4')

        l1 = request.POST.get('l1')
        l2 = request.POST.get('l2')
        l3 = request.POST.get('l3')
        l4 = request.POST.get('l4')

        non_statndard = False
        try:
            non_statndard = (request.POST.get('standard_row') == "9999")
        except:
            pass

        #init_dict = {'Order' : order, 'Profile': profile}

        form = OrderLineForm(data=request.POST)

        errormessage =''

        condition = check_condition(d1,d2,d3,d4,l1,l2,l3,l4,profile.Restriction)
        if not condition:
            errormessage = ugettext(profile.RestrictionError)

        if condition and profile.NumberOfMaterial > 0:
            condition = request.POST.get('Material1')
            errormessage = ugettext('Material') + ' 1 '+ ugettext('is missed')
        if condition and profile.NumberOfMaterial > 1 :
            condition = condition and request.POST.get('Material2')
            errormessage = ugettext('Material') + ' 2 ' + ugettext('is missed')
        if condition and profile.NumberOfMaterial > 2 :
            condition = condition and request.POST.get('Material3')
            errormessage = ugettext('Material') + ' 3 ' + ugettext('is missed')
        if condition and profile.NumberOfMaterial > 3 :
            condition = condition and request.POST.get('Material4')
            errormessage = ugettext('Material') + ' 4 ' + ugettext('is missed')

        if (form.is_valid() and condition) :

            line = form.save(commit=False)

            if d1 :
                line.D1 = float(d1)
            if d2 :
                line.D2 = float(d2)
            if d3 :
                line.D3 = float(d3)
            if d4 :
                line.D4 = float(d4)

            if l1 :
                line.L1 = float(l1)
            if l2 :
                line.L2 = float(l2)
            if l3 :
                line.L3 = float(l3)
            if l4 :
                line.L4 = float(l4)

            line.NonStandard = non_statndard

            line.Profile = profile

            line.save()

            context.update(action='/edit_order/')
            context.update(order=order)
            context.update(lines=lines)
            context.update(order_str=str(order))

            client = order.Client
            clientWS = suds_client.Client(PATH_2_SERVIСES, username=WEB_USER, password=WEB_USER_PASSWORD)
            try :
                result = clientWS.service.GetListOfSealDepartment(client.EDRPOU, client.Password)
                departments_list0 = json.loads(str(result))
                departments_list = departments_list0 [ 'Departments' ]
            except :
                departments_list = [ {"DepartmentName" : order.Department, "DepartmentCode" : order.DepartmentCode} ]
            context.update(departments_list = departments_list)

            request.session [ 'order_id' ] = order.id

            return render(request, 'edit_order.html', context)
        else:
            context = {
                'profile' : profile,
                'profile_txt': profile_txt,
                'form' : form,
                'current_language' : request.session.get(LANGUAGE_SESSION_KEY),
                'Description' : profile.Description,
                'imagefile' : profile.EditImageFileName,
                'order' : order,
                'd1' : str(d1).replace(',', '.'),
                'd2' : str(d2).replace(',', '.'),
                'd3' : str(d3).replace(',', '.'),
                'd4' : str(d4).replace(',', '.'),
                'l1' : str(l1).replace(',', '.'),
                'l2' : str(l2).replace(',', '.'),
                'l3' : str(l3).replace(',', '.'),
                'l4' : str(l4).replace(',', '.'),
                'errormessage' : errormessage,
            }
            return render(request, 'new_seal.html', context)

    context = {}

    context.update(action='/edit_order/')
    context.update(order=order)
    context.update(lines=lines)
    context.update(order_str=str(order))

    client = order.Client
    clientWS = suds_client.Client(PATH_2_SERVIСES, username=WEB_USER, password=WEB_USER_PASSWORD)
    try :
        result = clientWS.service.GetListOfSealDepartment(client.EDRPOU, client.Password)
        departments_list0 = json.loads(str(result))
        departments_list = departments_list0 [ 'Departments' ]
    except :
        departments_list = [ {"DepartmentName" : order.Department, "DepartmentCode" : order.DepartmentCode} ]
    context.update(departments_list=departments_list)

    return render(request, 'edit_order.html', context)

def get_invoice(request, order_id) :
    if not request.user.is_authenticated:
        return redirect('/')

    order = Orders.objects.get(id=order_id)

    try:
        client = suds_client.Client(PATH_2_SERVIСES, username = WEB_USER, password = WEB_USER_PASSWORD)

        req_text = {
            "EDRPOU" : order.Client.EDRPOU,
            "OrderNum" : order.Number,
            "UserName" : request.user.first_name,
            "OrderDate" : order.Date.strftime("%Y%m%d"),
        }

        result = client.service.GetInvoice(order.Client.EDRPOU, order.Client.Password, str(req_text))
        answer_dict0 = json.loads(str(result))
        answer = answer_dict0['Data']

        FileType = answer_dict0 [ 'FileType' ]

        buffer = BytesIO.BytesIO()
        content = base64.b64decode(answer)
        buffer.write(content)

        if FileType.upper() == 'PDF' :
            response = HttpResponse(
                buffer.getvalue(),
                content_type="application/pdf",
            )
            response [ 'Content-Disposition' ] = 'inline;filename=some_file.pdf'

        if FileType.upper() in ('XLS', 'XLS95', 'XLS97', 'XLSX'):
            response = HttpResponse(
                buffer.getvalue(),
                content_type="application/vnd.ms-excel",
            )
            response [ 'Content-Disposition' ] = 'inline;filename=some_file.xls'


        #response = HttpResponse(
        #    buffer.getvalue(),
        #    content_type="application/pdf",
        #)
        #response [ 'Content-Disposition' ] = 'inline;filename=some_file.pdf'

        return response

    except Exception as e:
        return HttpResponse("Get invoice:" + str(e))

    return list_order(request)

def choose_type(request):

    if not request.user.is_authenticated:
        return redirect('/')

    profile_types = ProfileTypes.objects.all()

    context = {
        'current_language' : request.session.get(LANGUAGE_SESSION_KEY),
        'profile_types' : profile_types,
    }

    return render(request, 'choose_type.html', context)

def choose_profile(request):
    if request.method == 'GET' :
        profile_type_id = request.GET.get('type')
        profile_type = ProfileTypes.objects.get(id = int(profile_type_id))

        profiles = Profiles.objects.filter(ProfileType = profile_type).order_by('Name')

    context = {
        'current_language' : request.session.get(LANGUAGE_SESSION_KEY),
        'profiles' : profiles,
    }

    return render(request, 'choose_profile.html',
                  context)

def change_form_pay(request):
    if request.is_ajax():
        order_id = int(request.GET.get('order_id'))
        form_pay = request.GET.get('form_pay')
        order_type = request.GET.get('type')

        if order_type == 'seal':
            Orders.objects.filter(id=order_id).update(FormOfPayment = form_pay)
        elif order_type == 'delivery':
            DeliveryOrders.objects.filter(id=order_id).update(FormOfPayment = form_pay)

        return JsonResponse({
            'status': True
        })

def update_line(request, line_id):

    if not request.user.is_authenticated:
        return redirect('/')

    line = OrderLines.objects.get(id = line_id)
    order = Orders.objects.get(id = line.Order_id)
    profile = Profiles.objects.get(id = line.Profile_id)
    profile_txt = str(profile)

    lines = OrderLines.objects.filter(Order_id = order.id)

    if request.method == 'POST':
        d1 = request.POST.get('d1')
        d2 = request.POST.get('d2')
        d3 = request.POST.get('d3')
        d4 = request.POST.get('d4')

        l1 = request.POST.get('l1')
        l2 = request.POST.get('l2')
        l3 = request.POST.get('l3')
        l4 = request.POST.get('l4')

        qty = request.POST.get('Quantity')

        non_statndard = False
        try:
            non_statndard = (request.POST.get('standard_row') == "9999")
        except:
            pass

        form = OrderLineForm(data=request.POST)

        errormessage = ''
        condition = check_condition(d1,d2,d3,d4,l1,l2,l3,l4,profile.Restriction)
        if not condition:
            errormessage = profile.RestrictionError

        if condition and profile.NumberOfMaterial > 0 :
            condition = condition and request.POST.get('Material1')
            errormessage = ugettext('Material') + ' 1 ' + ugettext('is missed')
        if condition and profile.NumberOfMaterial > 1 :
            condition = condition and request.POST.get('Material2')
            errormessage = ugettext('Material') + ' 2 ' + ugettext('is missed')
        if condition and profile.NumberOfMaterial > 2 :
            condition = condition and request.POST.get('Material3')
            errormessage = ugettext('Material') + ' 3 ' + ugettext('is missed')
        if condition and profile.NumberOfMaterial > 3 :
            condition = condition and request.POST.get('Material4')
            errormessage = ugettext('Material') + ' 4 ' + ugettext('is missed')

        if form.is_valid() and condition:

            #line = form.save(commit=False)

            if d1 :
                line.D1 = float(d1)
            if d2 :
                line.D2 = float(d2)
            if d3 :
                line.D3 = float(d3)
            if d4 :
                line.D4 = float(d4)

            if l1 :
                line.L1 = float(l1)
            if l2 :
                line.L2 = float(l2)
            if l3 :
                line.L3 = float(l3)
            if l4 :
                line.L4 = float(l4)

            line.Quantity = qty

            line.Profile = profile
            line.Comment = request.POST.get('Comment')
            line.K1 = request.POST.get('K1')
            line.K2 = request.POST.get('K2')
            line.K3 = request.POST.get('K3')
            line.K4 = request.POST.get('K4')

            line.NonStandard = non_statndard

            if profile.NumberOfMaterial > 0:
                line.Material1 = Materials.objects.get(id = request.POST.get('Material1'))
            if profile.NumberOfMaterial > 1 :
                line.Material2 = Materials.objects.get(id=request.POST.get('Material2'))
            if profile.NumberOfMaterial > 2 :
                line.Material3 = Materials.objects.get(id=request.POST.get('Material3'))
            if profile.NumberOfMaterial > 3 :
                line.Material4 = Materials.objects.get(id=request.POST.get('Material4'))

            line.save()

            context = {}
            context.update(action='/edit_order/')
            context.update(order=order)
            context.update(lines=lines)
            context.update(order_str=str(order))

            client = order.Client
            clientWS = suds_client.Client(PATH_2_SERVIСES, username=WEB_USER, password=WEB_USER_PASSWORD)
            try :
                result = clientWS.service.GetListOfSealDepartment(client.EDRPOU, client.Password)
                departments_list0 = json.loads(str(result))
                departments_list = departments_list0 [ 'Departments' ]
            except :
                departments_list = [ {"DepartmentName" : order.Department, "DepartmentCode" : order.DepartmentCode} ]
            context.update(departments_list = departments_list)

            request.session [ 'order_id' ] = order.id

            return render(request, 'edit_order.html', context)
        else:
            context = {
                'profile' : profile,
                'profile_txt': profile_txt,
                'form' : form,
                'current_language' : request.session.get(LANGUAGE_SESSION_KEY),
                'imagefile' : line.Profile.EditImageFileName,
                'order' : line.Order,
                'd1' : str(line.D1).replace(',', '.'),
                'd2' : str(line.D2).replace(',', '.'),
                'd3' : str(line.D3).replace(',', '.'),
                'd4' : str(line.D4).replace(',', '.'),
                'l1' : str(line.L1).replace(',', '.'),
                'l2' : str(line.L2).replace(',', '.'),
                'l3' : str(line.L3).replace(',', '.'),
                'l4' : str(line.L4).replace(',', '.'),
                'line_id' : line.id,
                'errors' : form.errors,
                'Description' : line.Profile.Description,
                'errormessage' : errormessage,
            }
            return render(request, 'new_seal.html', context)

    context = {}

    context.update(action='/edit_order/')
    context.update(order=order)
    context.update(lines=lines)
    context.update(order_str=str(order))

    client = order.Client
    clientWS = suds_client.Client(PATH_2_SERVIСES, username=WEB_USER, password=WEB_USER_PASSWORD)
    try :
        result = clientWS.service.GetListOfSealDepartment(client.EDRPOU, client.Password)
        departments_list0 = json.loads(str(result))
        departments_list = departments_list0 [ 'Departments' ]
    except :
        departments_list = [ {"DepartmentName" : order.Department, "DepartmentCode" : order.DepartmentCode} ]
    context.update(departments_list=departments_list)

    return render(request, 'edit_order.html', context)



def copy_line(request, line_id):

    if not request.user.is_authenticated:
        return redirect('/')

    line = OrderLines.objects.get(id = line_id)
    line.id = None
    line.save()

    order = Orders.objects.get(id = line.Order_id)
    standard_rows = StabdardRows.objects.filter(Profile=line.Profile)

    init_dict = {'Order' : line.Order,
                 'Profile' : line.Profile,
                 'D1': line.D1,
                 'D2' : line.D2,
                 'D3' : line.D3,
                 'D4' : line.D4,
                 'L1' : line.L1,
                 'L2' : line.L2,
                 'L3' : line.L3,
                 'L4' : line.L4,
                 'Quantity': line.Quantity,
                 'Material1': line.Material1,
                 'Material2' : line.Material2,
                 'Material3' : line.Material3,
                 'Material4' : line.Material4,
                 'K1' : line.K1,
                 'K2' : line.K2,
                 'K3' : line.K3,
                 'K4' : line.K4,
                 'Comment' : line.Comment,
                 'NonStandard': line.NonStandard,
                 'standard_rows': standard_rows,
                 }

    context = {
        'imagefile' : line.Profile.EditImageFileName,
        'profile_txt' : str(line.Profile),
        'profile' : line.Profile,
        'form' : OrderLineForm(init_dict),
        'current_language' : request.session.get(LANGUAGE_SESSION_KEY),
        'order' : line.Order,
        'd1' : str(line.D1).replace(',','.'),
        'd2' : str(line.D2).replace(',','.'),
        'd3' : str(line.D3).replace(',','.'),
        'd4' : str(line.D4).replace(',','.'),
        'l1' : str(line.L1).replace(',','.'),
        'l2' : str(line.L2).replace(',','.'),
        'l3' : str(line.L3).replace(',','.'),
        'l4' : str(line.L4).replace(',','.'),
        'line_id' : line.id,
        'Description' : line.Profile.Description,
        'NonStandard' : line.NonStandard,
        'standard_rows' : standard_rows,
    }
    #if form.is_valid() :

    return render(request, 'new_seal.html', context)

#    lines = OrderLines.objects.filter(Order_id=order.id)
#
#    context = {}
#
#    context.update(action='/edit_order/')
#    context.update(order=order)
#    context.update(lines=lines)
#    context.update(order_str=str(order))
#
#    client = order.Client
#    clientWS = suds_client.Client(PATH_2_SERVIСES, username=WEB_USER, password=WEB_USER_PASSWORD)
#    try :
#        result = clientWS.service.GetListOfSealDepartment(client.EDRPOU, client.Password)
#        departments_list0 = json.loads(str(result))
#        departments_list = departments_list0 [ 'Departments' ]
#    except :
#        departments_list = [ {"DepartmentName" : order.Department, "DepartmentCode" : order.DepartmentCode} ]
#    context.update(departments_list=departments_list)
#
#    return render(request, 'edit_order.html', context)

def materials(request):
    return render(request, 'materials.html', {'current_language' : request.session.get(LANGUAGE_SESSION_KEY)})

def debt(request):

    try:
        client = suds_client.Client(PATH_2_SERVIСES, username = WEB_USER, password = WEB_USER_PASSWORD)

        client_org = ClientUsers.objects.get(UserId_id=request.user.id).Client
        result = client.service.GetDebt(client_org.EDRPOU, client_org.Password, "")
        answer_dict0 = json.loads(str(result))

        total_debt = answer_dict0 [ 'Sum' ]
        QtyDaysBT = answer_dict0 [ 'QtyDaysBT' ]
        DealBT = answer_dict0 [ 'DealBT' ]
        SumBT = answer_dict0 [ 'SumBT' ]
        QtyDaysCP = answer_dict0 [ 'QtyDaysCP' ]
        DealCP = answer_dict0 [ 'DealCP' ]
        SumCP = answer_dict0 [ 'SumCP' ]

        BT_overdue = "" + ugettext('BT debt is UAH') + str(SumBT) + ' ' + ugettext('UAH')
        if SumBT > 0:
            BT_overdue = "" + ugettext('BT debt is UAH') + str(SumBT) + ' ' + ugettext('UAH') + ', ' + str(QtyDaysBT) + ugettext(
                ' days ') + ugettext(' deal ') + '"' + str(DealBT) + '"'

        CP_overdue = "" + ugettext('CP debt is UAH') + str(SumCP) + ' ' + ugettext('UAH')
        if SumCP > 0:
            CP_overdue = "" + ugettext('CP debt is UAH') + str(SumCP) + ' ' + ugettext('UAH') + ', ' + str(QtyDaysCP) + ugettext(
                ' days ') + ugettext(' deal ') + '"' + str(DealCP) + '"'

        return render(request,'home_page.html', {
            'total_debt': str(total_debt) + ' ' + ugettext('UAH'),
            'BT_overdue' : BT_overdue,
            'CP_overdue' : CP_overdue,
            'current_language' : request.session.get(LANGUAGE_SESSION_KEY),
        })

    except Exception as e:
        return HttpResponse("Get debt:" + str(e))

def ajax_get_standard_size(request):
    if request.is_ajax():
        id = int(request.GET.get('id'))
        standard_row = StabdardRows.objects.get(id=id)
        return JsonResponse({
            'l1': standard_row.L1,
            'l2' : standard_row.L2,
        })

def ajax_change_delivery(request):
    if request.is_ajax():

        try:
            order_id = int(request.GET.get('order_id'))
            shipping = request.GET.get('shipping')
            city = request.GET.get('city')
            recipient = request.GET.get('recipient')
            person = request.GET.get('person')
            phone = request.GET.get('phone')
            order_type = request.GET.get('order_type')
            shipping = int(shipping)

            if order_type == 'delivery':
                DeliveryOrders.objects.filter(id=order_id).update(Shipping = shipping)

                if shipping == 1:
                    DeliveryOrders.objects.filter(id=order_id).update(City=city, Recipient = recipient, Person = person, Phone = phone)
                else:
                    DeliveryOrders.objects.filter(id=order_id).update(City='', Recipient = '', Person = '', Phone = '')
            else:
                Orders.objects.filter(id=order_id).update(Shipping = shipping)

                if shipping == 1:
                    Orders.objects.filter(id=order_id).update(City=city, Recipient = recipient, Person = person, Phone = phone)
                else:
                    Orders.objects.filter(id=order_id).update(City='', Recipient = '', Person = '', Phone = '')

            return JsonResponse({
                'status': True
            })
        except:
            return JsonResponse({
                'status': False
            })


def ajax_change_comment(request):
    if request.is_ajax():
        try:
            order_id = int(request.GET.get('order_id'))
            comment = request.GET.get('comment')
            order_type = request.GET.get('order_type')

            if comment == None:
                comment = ""

            if order_type == 'delivery':
                DeliveryOrders.objects.filter(id=order_id).update(Comment=comment)
            else:
                Orders.objects.filter(id=order_id).update(Comment = comment)

            return JsonResponse({
                'status': True
            })
        except:
            return JsonResponse({
                'status': False
            })


def copy_order(request, order_id):

    if not request.user.is_authenticated:
        return redirect('/')

    order = Orders.objects.get(id = order_id)
    order.id = None
    order.Loaded = False
    order.Summa = 0
    order.Number = ''
    order.Date = None
    order.Status = 'calculation'
    order.BillNumber = ''
    order.BillDate = None
    order.ApprovalStatus = 0
    order.IsInvoice = False
    order.ConsignmentNote = ''
    order.Author = " ".join([request.user.first_name, request.user.last_name])[:29]

    order.save()

    lines = OrderLines.objects.filter(Order_id=order_id)

    for line in lines:
        line.id = None
        line.Order_id = order.id
        line.save()

    context = {
        'action' : '/edit_order/',
        'order' : order,
        'lines' : lines,
        'order_srt' : str(order),
        'current_language' : request.session.get(LANGUAGE_SESSION_KEY),
    }
    request.session [ 'order_id' ] = order.id

    client = order.Client

    clientWS = suds_client.Client(PATH_2_SERVIСES, username=WEB_USER, password=WEB_USER_PASSWORD)

    try :
        result = clientWS.service.GetListOfSealDepartment(client.EDRPOU, client.Password)
        departments_list0 = json.loads(str(result))
        departments_list = departments_list0 [ 'Departments' ]

    except :
        departments_list = [ {"DepartmentName" : order.Department, "DepartmentCode" : order.DepartmentCode} ]

    context.update(departments_list = departments_list)

    return render(request, 'edit_order.html', context)


def delete_order(request, order_id):
    Orders.objects.get(id=order_id).delete()
    return list_order(request)

def paid(request, order_id) :
    if not request.user.is_authenticated:
        return redirect('/')

    type_o = request.GET.get('type_o')
    if type_o == "delivery":
        order = DeliveryOrders.objects.get(id=order_id)
        lines = DeliveryOrderLines.objects.filter(DeliveryOrder_id=order.id)
    else:
        order = Orders.objects.get(id=order_id)
        lines = OrderLines.objects.filter(Order_id=order.id)

    try:
        client = suds_client.Client(PATH_2_SERVIСES, username = WEB_USER, password = WEB_USER_PASSWORD)

        req_text = {
            "EDRPOU" : order.Client.EDRPOU,
            "OrderNum" : str(order_id),
            "UserName" : request.user.first_name,
            "CurrentUser" : request.user.first_name,
            "OrderDate" : order.SysDate.strftime("%Y%m%d"),
        }

        result = client.service.PayConfirmationRequest(order.Client.EDRPOU, order.Client.Password, str(req_text))
        answer_dict0 = json.loads(str(result))
        answer = answer_dict0['Return']

        msg = ""
        errormsg = ""

        if answer == "Request was successfully sent":
            msg = ugettext('Request was successfully sent')
        else:
            errormsg = ugettext('An error occurred while sending request ') + str(order_id) + ' :' + ugettext(answer)

    except Exception as e:
        return HttpResponse("Get bill:" + str(e))

    if type_o == "delivery":
        delivery_order = DeliveryOrders.objects.get(id=order_id)
        delivery_lines = DeliveryOrderLines.objects.filter(DeliveryOrder_id=order_id)

        context = {
            'action' : '/list_delivery_order/',
            'order' : delivery_order,
            'lines' : delivery_lines,
            'order_srt' : str(delivery_order),
            'current_language' : request.session.get(LANGUAGE_SESSION_KEY),
        }
        request.session [ 'delivery_order_id' ] = delivery_order.id
        return render(request, 'edit_delivery_order.html', context)

    else:
        context = {
            'action' : '/edit_order/',
            'order' : order,
            'lines' : lines,
            'msg' : msg,
            'errormsg' : errormsg,
            'order_srt' : str(order),
            'current_language' : request.session.get(LANGUAGE_SESSION_KEY),
        }
        request.session [ 'order_id' ] = order.id

        client = order.Client
        clientWS = suds_client.Client(PATH_2_SERVIСES, username=WEB_USER, password=WEB_USER_PASSWORD)
        try :
            result = clientWS.service.GetListOfSealDepartment(client.EDRPOU, client.Password)
            departments_list0 = json.loads(str(result))
            departments_list = departments_list0 [ 'Departments' ]
        except :
            departments_list = [ {"DepartmentName" : order.Department, "DepartmentCode" : order.DepartmentCode} ]
        context.update(departments_list = departments_list)

        return render(request, 'edit_order.html', context)

def ajax_change_department(request):
    if request.is_ajax():
        try:
            order_id = int(request.GET.get('order_id'))
            department = request.GET.get('department')

            client = ClientUsers.objects.get(UserId_id=request.user.id).Client

            clientWS = suds_client.Client(PATH_2_SERVIСES, username=WEB_USER, password=WEB_USER_PASSWORD)

            try :
                result = clientWS.service.GetListOfSealDepartment(client.EDRPOU, client.Password)
                departments_list0 = json.loads(str(result))
                departments_list = departments_list0 [ 'Departments' ]

                for dep in departments_list:
                    if dep['DepartmentName'] == department:
                        Orders.objects.filter(id=order_id).update(Department = dep['DepartmentName'], DepartmentCode = dep['DepartmentCode'])
            except :
                pass

            return JsonResponse({
                'status': True
            })
        except:
            return JsonResponse({
                'status': False
            })

def new_delivery_order(request):

    if not request.user.is_authenticated:
        return redirect('/')

    client = ClientUsers.objects.get(UserId_id = request.user.id).Client

    default_data = {
        'Client' : client,
        'Status' : 'calculation',
        'Date' : date.today(),
    }

    if client.Shipping == 1 :
        default_data.update(Shipping=client.Shipping)
        default_data.update(City=client.City)
        default_data.update(Person=client.Person)
        default_data.update(Phone=client.Phone)
        default_data.update(Recipient=client.Recipient)
    elif client.Shipping == 5 :
        default_data.update(Shipping=client.Shipping)

    new_delivery_order = DeliveryOrders.objects.create(**default_data)

    author = " ".join([request.user.first_name, request.user.last_name])
    author = author[:29]

    DeliveryOrders.objects.filter(id=new_delivery_order.id).update(Author = author, Department=client.Department, DepartmentCode=client.DepartmentCode)

    if client.FormOfPayment == "CP":
        DeliveryOrders.objects.filter(id=new_delivery_order.id).update(FormOfPayment="CP")

    request.session['delivery_order_id'] = new_delivery_order.id
    delivery_lines = DeliveryOrderLines.objects.filter(DeliveryOrder_id=new_delivery_order.id)

    delivery_lines = delivery_lines.values()
    for delivery_line in delivery_lines:
        delivery_line.update(departments = [])

    context = {
        'action': '/list_delivery_order/',
        'form' : DeliveryOrderForm(),
        'order': new_delivery_order,
        'lines': delivery_lines,
        'order_srt': str(new_delivery_order),
        'current_language': request.session.get(LANGUAGE_SESSION_KEY),
        'form_of_payment': "CP" if client.FormOfPayment == "CP" else "BT"
    }

    return render(request, 'edit_delivery_order.html', context)

def choose_category(request):

    if not request.user.is_authenticated:
        return redirect('/')

    categories = Categories.objects.filter(Folder_id__isnull=True)

    context = {
        'current_language' : request.session.get(LANGUAGE_SESSION_KEY),
        'categories' : categories,
    }

    return render(request, 'choose_category.html', context)

def update_nomenclature_list(nomenclatures, CP, department_code, client):

    clientWS = suds_client.Client(PATH_2_SERVIСES, username=WEB_USER, password=WEB_USER_PASSWORD)

    article_string = ""
    for elem in nomenclatures:
        article_string = article_string + ";" + elem['Article']
    article_string = article_string[1:]

    req_dic = {
        "EDRPOU" : client.EDRPOU,
        "UserName" : "",
        "CP" : CP,
        "DepartmentCode" : "",
        "NoPrice" : "0",
        "FilterCategory" : "",
        "FilterName" : "",
        "Nomenclatures" : article_string
    }

    try :
        result = clientWS.service.CheckNomenclatureList(client.EDRPOU, client.Password, str(req_dic))

        answer_dict0 = json.loads(str(result))
        answer_list = answer_dict0 [ 'Strings' ]

        for elem in answer_list :

            for nomenclature in nomenclatures:
                if nomenclature['Article'] == elem['Article']:
                    nomenclature.update(Price=elem [ 'Price' ])
                    nomenclature.update(Count=elem [ 'Quantity' ])
                    try :
                        #content = base64.b64decode(elem [ 'PictureData' ])

                        #register = template.Library()
                        #@register.filter
                        #def bin_2_img ( _bin ) :
                        #content = b64encode(elem [ 'PictureData' ]).decode('utf-8')
                        content = elem [ 'PictureData' ]
                        nomenclature.update(Picture=content)
                    except :
                        nomenclature.update(Picture="")

                    properties = []
                    for elem1 in elem ['Properties']:
                        properties.append({'Property': elem1['Property'], 'Value':elem1['Value']})
                    nomenclature.update(Properties=properties)

                    storages = []
                    for elem1 in elem ['Storages']:
                        storages.append({'Storage': elem1['Storage'], 'Count':elem1['Quantity']})
                    nomenclature.update(Storages=storages)
                    #print("nomenclature=", nomenclature)
                    break
    except Exception as e:
        print(type(e), e)
        errormessage = ugettext("The Hydrohouse base is not available. Please try again later")

    return nomenclatures


def choose_subcategory(request):
    if request.method == 'GET' :
        category_id = request.GET.get('category')
        category = Categories.objects.get(id=int(category_id))
        subcategories = Categories.objects.filter(Folder_id=category.id).order_by('Name')
        if subcategories.count() > 0:
            context = {
            'current_language' : request.session.get(LANGUAGE_SESSION_KEY),
            'categories' : subcategories,
            }

            return render(request, 'choose_category.html',
                  context)
        else:

            client = ClientUsers.objects.get(UserId_id=request.user.id).Client
            clientWS = suds_client.Client(PATH_2_SERVIСES, username=WEB_USER, password=WEB_USER_PASSWORD)
            delivery_order_id = request.session.get('delivery_order_id')
            delivery_order = DeliveryOrders.objects.get(id=delivery_order_id)
            CP = "1" if delivery_order.FormOfPayment == "CP" else "0"
            department_code = delivery_order.DepartmentCode

            req_dic = {
                "EDRPOU" : client.EDRPOU,
                "UserName" : request.user.first_name,
                "CP" : CP,
                "DepartmentCode" : department_code,
                "NoPrice" : "1",
                "FilterCategory" : category.Code1c,
                "FilterName": "",
            }

            try :
                result = clientWS.service.CheckNomenclatureList(client.EDRPOU, client.Password, str(req_dic))

                answer_dict0 = json.loads(str(result))
                answer_list = answer_dict0 [ 'Strings' ]

                nomenclatures = []

                for elem in answer_list :
                    nomenclature = {}
                    nomenclature.update(Name=elem['Name'])
                    nomenclature.update(Article=elem['Article'])
                    nomenclature.update(UnitName=elem['UnitName'])
                    nomenclature.update(Count=elem['Quantity'])

                    nomenclatures.append(nomenclature)

            except :
                errormessage = ugettext("The Hydrohouse base is not available. Please try again later")
                pass

            paginator = Paginator(nomenclatures, 8)
            page = request.GET.get('page')

            try :
                nomenclatures = paginator.page(page)
                nomenclatures = update_nomenclature_list(nomenclatures, CP, department_code, client)
            except PageNotAnInteger :
                # If page is not an integer, deliver first page.
                nomenclatures = paginator.page(1)
                nomenclatures = update_nomenclature_list(nomenclatures, CP, department_code, client)
            except EmptyPage :
                # If page is out of range (e.g. 9999), deliver last page of results.
                nomenclatures = paginator.page(paginator.num_pages)
                nomenclatures = update_nomenclature_list(nomenclatures, CP, department_code, client)

            context = {
                'current_language' : request.session.get(LANGUAGE_SESSION_KEY),
            }
            context.update(nomenclatures=nomenclatures)
            context.update(category=category_id)

            return render(request, 'choose_component.html', context)

    return redirect('/')

def add_delivery_line(request):
    delivery_order_id = request.session.get('delivery_order_id')

    if request.method == 'GET' :
        article = request.GET.get('article')

        nomenclature = request.GET.get('name')
        price = str(request.GET.get('price')).replace(',','.')

        delivery_line = DeliveryOrderLines(Nomenclature = nomenclature, Article = article,
                                           Price = float(price), Quantity = 1, Summa = float(price),
                                           DeliveryOrder_id = delivery_order_id, Department = "",)
        delivery_line.save()

    delivery_order = DeliveryOrders.objects.get(id=delivery_order_id)
    delivery_lines = DeliveryOrderLines.objects.filter(DeliveryOrder_id=delivery_order_id)

    doc_sum = 0
    for delivery_line in delivery_lines:
        doc_sum = doc_sum + delivery_line.Summa

    delivery_order.Summa = doc_sum
    delivery_order.save()

    context = {
            'action' : '/list_delivery_order/',
            'order' : delivery_order,
            'lines' : delivery_lines,
            'order_srt' : str(delivery_order),
            'current_language' : request.session.get(LANGUAGE_SESSION_KEY),
    }
    request.session [ 'delivery_order_id' ] = delivery_order.id

    return render(request, 'edit_delivery_order.html', context)

def ajax_delete_delivery_line(request):
    if request.is_ajax():
        line_id = int(request.GET.get('id'))
        DeliveryOrderLines.objects.get(id = line_id).delete()
        return JsonResponse({
            'status': True
        })


def list_delivery_order(request, errormessage = None, successmessage = None):
    import datetime
    context = {
        'current_language' : request.session.get(LANGUAGE_SESSION_KEY),
        'is_mobile': get_user_agent(request).is_mobile
    }

    list_filter = request.GET.get('filter')

    client_org = ClientUsers.objects.get(UserId_id=request.user.id).Client

    if 'search' in request.GET :
        search_str = request.GET [ 'search' ]
        delivery_orders = DeliveryOrders.objects.filter(Client=client_org, Loaded=True).filter(Comment__icontains=search_str).exclude(IsInvoice=True).exclude(ApprovalStatus=-1)
    else:
        search_str = ''
        delivery_orders = DeliveryOrders.objects.filter(Client=client_org, Loaded=True).exclude(IsInvoice=True).exclude(ApprovalStatus=-1)

    req_orders = []
    for delivery_order in delivery_orders:
        req_order = {
            'OrderNum': str(delivery_order.id),
            'OrderDate' : delivery_order.SysDate.strftime("%Y%m%d"),
        }
        req_orders.append(req_order)

    client = suds_client.Client(PATH_2_SERVIСES, username = WEB_USER, password = WEB_USER_PASSWORD)

    try:
        result = client.service.UpdateListDeliveryOrder(client_org.EDRPOU, client_org.Password, str(req_orders))

        answer_dict0 = json.loads(str(result))
        answer_list = answer_dict0 [ 'Strings' ]

        for elem in answer_list :

            delivery_order_id = int(elem["Id"])
            delivery_order = DeliveryOrders.objects.get(id=delivery_order_id)

            ordersHH = DeliveryOrdersHH.objects.filter(DeliveryOrder_id=delivery_order_id)

            if ordersHH.count() > 0:
                for orderHH in ordersHH:
                    orderHH.delete()

            all_invoices = None
            min_status = None

            for elem_order in elem [ "Orders" ] :
                sum_doc = round(float(elem_order[ "OrderSum" ]) , 2)

                status_str = elem_order [ 'Status' ]

                if status_str == 'Калькуляция' :
                    status = 'calculation'
                elif status_str == 'Заказ' :
                    status = 'order'
                elif status_str == "Передано в производство" :
                    status = 'ready_work'
                elif status_str == 'В работе' :
                    status = 'in_progress'
                elif status_str == 'Выполнено' :
                    status = 'complete'

                if min_status is None :
                    min_status = status

                if status == 'calculation':
                    min_status = status
                if status == 'order' and not min_status == 'order' and not min_status == 'calculation':
                    min_status = status
                if status == 'ready_work' and (min_status == 'in_progress' or  min_status == 'complete') :
                    min_status = status
                if status == 'in_progress' and  min_status == 'complete' :
                    min_status = status

                try :
                    delivery_order_date = datetime.datetime(int(elem_order [ 'OrderDate' ] [ 0 :4 ]), int(elem_order [ 'OrderDate' ] [ 4 :6 ]),
                                           int(elem_order [ 'OrderDate' ] [ 6 :8 ]))
                except :
                    delivery_order_date = None

                try :
                    bill_date = datetime.datetime(int(elem_order [ 'BillDate' ] [ 0 :4 ]), int(elem_order [ 'BillDate' ] [ 4 :6 ]),
                                      int(elem_order [ 'BillDate' ] [ 6 :8 ]))
                except :
                    bill_date = None

                is_invoice = False
                try :
                    is_invoice = not (elem_order [ 'InvoiceNum' ] == '')
                except :
                    pass

                if all_invoices is None:
                    all_invoices = is_invoice
                else:
                    all_invoices = (all_invoices and is_invoice)

                try:
                    bill_num = elem_order ['BillNum']
                except:
                    bill_num = None

                DeclarationNum = elem_order['DeclarationNum']
                Department = elem_order['Store']

                try:
                    DeliveryOrdersHH.objects.create(DeliveryOrder=delivery_order, Date=delivery_order_date,
                                                  Status=status, Number = elem_order ['OrderNum'],
                                                  Summa = sum_doc, BillNumber=bill_num, BillDate=bill_date,
                                                  IsInvoice=is_invoice, ConsignmentNote=DeclarationNum,
                                                  Department = Department)
                except:
                    pass

            if not all_invoices is None:
                if all_invoices:
                    delivery_order.IsInvoice = True
                    delivery_order.save()

            if not min_status is None:
                delivery_order.Status = min_status
                delivery_order.save()

    except:
        errormessage = ugettext("The Hydrohouse base is not available. Please try again later")
        pass

    if search_str == '' :
        if list_filter == 'New':
            delivery_orders = DeliveryOrders.objects.filter(Client = client_org, Loaded=False).order_by('-id')
        elif list_filter == 'Loaded':
            delivery_orders = DeliveryOrders.objects.filter(Client = client_org, Loaded=True, Status='calculation').order_by('-id')
        elif list_filter == 'ToApprove':
            delivery_orders = DeliveryOrders.objects.filter(Client = client_org, ApprovalStatus = 0).order_by('-id').exclude(Status='calculation')
        elif list_filter == 'InWork':
            delivery_orders = DeliveryOrders.objects.filter(Client = client_org, ApprovalStatus = 1).order_by('-id').exclude(Status='complete').exclude(Status='calculation')
        elif list_filter == 'Complete':
            delivery_orders = DeliveryOrders.objects.filter(Client = client_org, Status='complete').order_by('-id')
        else:
            delivery_orders = DeliveryOrders.objects.filter(Client=client_org).order_by('-id')
            list_filter = 'All'
    else:
        delivery_orders = DeliveryOrders.objects.filter(Client=client_org).filter(Comment__icontains=search_str).order_by('-id')
        list_filter = 'All'

    context.update(filter=list_filter)
    context.update(search_str=search_str)

    paginator = Paginator(delivery_orders, 10)
    page = request.GET.get('page')

    try :
        delivery_orders = paginator.page(page)
    except PageNotAnInteger :
        # If page is not an integer, deliver first page.
        delivery_orders = paginator.page(1)
    except EmptyPage :
        # If page is out of range (e.g. 9999), deliver last page of results.
        delivery_orders = paginator.page(paginator.num_pages)

    context.update(orders=delivery_orders)
    if errormessage:
        context.update(errormessage=errormessage)
    if successmessage:
        context.update(successmessage=successmessage)

    return render(request, 'list_delivery_order.html', context)

def edit_delivery_order(request, order_id):

    if not request.user.is_authenticated:
        return redirect('/')

    delivery_order_id = order_id

    delivery_order = DeliveryOrders.objects.get(id = delivery_order_id)
    delivery_lines = DeliveryOrderLines.objects.filter(DeliveryOrder_id=delivery_order_id)

    delivery_lines = delivery_lines.values()
    for delivery_line in delivery_lines:
        delivery_line.update(departments = [{'Storage' : delivery_line['Department'],'Quantity' : ""},])

    ordersHH = DeliveryOrdersHH.objects.filter(DeliveryOrder_id=delivery_order_id)

    client = delivery_order.Client

    context = {
        'action': '/list_delivery_order/',
        'order' : delivery_order,
        'lines' : delivery_lines,
        'ordersHH' : ordersHH,
        'order_srt' : str(delivery_order),
        'current_language' : request.session.get(LANGUAGE_SESSION_KEY),
    }
    request.session['delivery_order_id'] = delivery_order.id
    return render(request, 'edit_delivery_order.html', context)


def ajax_update_delivery_line(request):

    if request.is_ajax():
        line_id = int(request.GET.get('id'))
        quantity = float(request.GET.get('quantity'))

        DeliveryOrderLines.objects.filter(id=line_id).update(Quantity=quantity, Summa = quantity * DeliveryOrderLines.objects.get(id = line_id).Price)

        delivery_order = DeliveryOrderLines.objects.get(id = line_id).DeliveryOrder
        delivery_lines = DeliveryOrderLines.objects.filter(DeliveryOrder_id=delivery_order.id)

        doc_sum = 0
        for delivery_line in delivery_lines :
            doc_sum = doc_sum + delivery_line.Summa

        delivery_order.Summa = doc_sum
        delivery_order.save()

        return JsonResponse({
            'status' : True
        })


def copy_delivery_order(request, order_id):

    if not request.user.is_authenticated:
        return redirect('/')

    delivery_order = DeliveryOrders.objects.get(id = order_id)
    delivery_lines = DeliveryOrderLines.objects.filter(DeliveryOrder=delivery_order)

    delivery_order.id = None
    delivery_order.Loaded = False
    delivery_order.Number = ''
    delivery_order.Date = date.today()
    delivery_order.Status = 'calculation'
    delivery_order.BillNumber = ''
    delivery_order.BillDate = None
    delivery_order.ApprovalStatus = 0
    delivery_order.IsInvoice = False
    delivery_order.ConsignmentNote = ''
    delivery_order.Author = " ".join([request.user.first_name, request.user.last_name])[:29]

    delivery_order.save()

    for delivery_line in delivery_lines:
        delivery_line.id = None
        delivery_line.Id_HH = 0
        delivery_line.DeliveryOrder = delivery_order
        delivery_line.Department = ""
        delivery_line.DepartmentCode = ""
        delivery_line.save()

    delivery_lines = delivery_lines.values()
    for delivery_line in delivery_lines:
        delivery_line.update(departments = [delivery_line['Department']])

    context = {
        'action' : '/list_delivery_order/',
        'order' : delivery_order,
        'lines' : delivery_lines,
        'order_srt' : str(delivery_order),
        'current_language' : request.session.get(LANGUAGE_SESSION_KEY),
    }
    request.session [ 'delivery_order_id' ] = delivery_order.id

    client = delivery_order.Client

    return render(request, 'edit_delivery_order.html', context)

def ajax_delete_delivery_order(request):
    if request.is_ajax():
        order_id = int(request.GET.get('id'))
        DeliveryOrders.objects.get(id=order_id).delete()
        return JsonResponse({
            'status': True
        })

def delete_delivery_order(request, order_id):
    DeliveryOrders.objects.get(id=order_id).delete()
    return list_delivery_order(request)


def search_components(request):
    if request.method == 'GET' :
        search_string = request.GET.get('search')

        if search_string == "":
            categories = Categories.objects.filter(Folder_id__isnull=True)
            context = {
                'current_language' : request.session.get(LANGUAGE_SESSION_KEY),
                'categories' : categories,
            }
            return render(request, 'choose_category.html', context)
        elif not (search_string is None):

            client = ClientUsers.objects.get(UserId_id=request.user.id).Client
            clientWS = suds_client.Client(PATH_2_SERVIСES, username=WEB_USER, password=WEB_USER_PASSWORD)
            delivery_order_id = request.session.get('delivery_order_id')
            delivery_order = DeliveryOrders.objects.get(id=delivery_order_id)
            CP = "1" if delivery_order.FormOfPayment == "CP" else "0"
            department_code = delivery_order.DepartmentCode

            req_dic = {
                "EDRPOU" : client.EDRPOU,
                "UserName" : request.user.first_name,
                "CP" : CP,
                "DepartmentCode" : department_code,
                "NoPrice" : "1",
                "FilterCategory" : "",
                "FilterName": search_string,
            }

            try :
                result = clientWS.service.CheckNomenclatureList(client.EDRPOU, client.Password, str(req_dic))

                answer_dict0 = json.loads(str(result))
                answer_list = answer_dict0 [ 'Strings' ]

                nomenclatures = []

                for elem in answer_list :
                    nomenclature = {}
                    nomenclature.update(Name=elem['Name'])
                    nomenclature.update(Article=elem['Article'])
                    nomenclature.update(UnitName=elem['UnitName'])
                    nomenclature.update(Count=elem['Quantity'])
                    nomenclatures.append(nomenclature)

            except :
                errormessage = ugettext("The Hydrohouse base is not available. Please try again later")
                pass
        else :
            categories = Categories.objects.filter(Folder_id__isnull=True)
            context = {
                'current_language' : request.session.get(LANGUAGE_SESSION_KEY),
                'categories' : categories,
            }
            return render(request, 'choose_category.html', context)

        paginator = Paginator(nomenclatures, 8)
        page = request.GET.get('page')

        try :
            nomenclatures = paginator.page(page)
            nomenclatures = update_nomenclature_list(nomenclatures, CP, department_code, client)
        except PageNotAnInteger :
            # If page is not an integer, deliver first page.
            nomenclatures = paginator.page(1)
            nomenclatures = update_nomenclature_list(nomenclatures, CP, department_code, client)
        except EmptyPage :
            # If page is out of range (e.g. 9999), deliver last page of results.
            nomenclatures = paginator.page(paginator.num_pages)
            nomenclatures = update_nomenclature_list(nomenclatures, CP, department_code, client)

        context = {
            'current_language' : request.session.get(LANGUAGE_SESSION_KEY),
        }
        context.update(nomenclatures=nomenclatures)
        context.update(search_str=search_string)

        return render(request, 'choose_component.html', context)

    return redirect('/')


def check_availability(request, order_id):

    if not request.user.is_authenticated:
        return redirect('/')

    client = ClientUsers.objects.get(UserId_id=request.user.id).Client
    delivery_order = DeliveryOrders.objects.get(id = order_id)
    delivery_lines = DeliveryOrderLines.objects.filter(DeliveryOrder=delivery_order)

    clientWS = suds_client.Client(PATH_2_SERVIСES, username=WEB_USER, password=WEB_USER_PASSWORD)

    stings = []
    number = 0

    for line in delivery_lines :
        number = number + 1

        stings.append(
        {
            "Row" : str(number),
            "RowId" : str((line.id if line.Id_HH == 0 else line.Id_HH) % 100000),
            "Article" : line.Article,
            "Unit" : "",
            "Quantity" : str(line.Quantity),
        })

    req_dic = {
        "EDRPOU" : client.EDRPOU,
        "UserName" : "",
        "OrderNum" : order_id,
        "OrderDate" : delivery_order.SysDate.strftime("%Y%m%d"),
        "Type" : "1",
        "Strings" : stings,
        "CP" : "1" if delivery_order.FormOfPayment == "CP" else "0",
    }

    try :
        result = clientWS.service.PostDeliveryOrder(client.EDRPOU, client.Password, str(req_dic))

        answer_dict0 = json.loads(str(result))
        answer_list = answer_dict0 [ 'Strings' ]
    except :
        pass

    delivery_lines = []
    departments = []

    sum_doc = 0

    for elem in answer_list :

        old_line = DeliveryOrderLines.objects.get(id = elem['OriginalRowId'])
        got_line, created = DeliveryOrderLines.objects.get_or_create(id=elem['RowId'], defaults={'DeliveryOrder': old_line.DeliveryOrder})
        if created:
            got_line.Nomenclature = old_line.Nomenclature
            got_line.Article = old_line.Article
        else:
            departments = []
            if 'Storages' in elem:
                for dep in elem ['Storages'] :
                    departments.append({'Storage': dep [ 'Storage' ], 'Quantity': "(" + str(dep['Quantity'])+" "+dep['UnitName']+")" })

        got_line.Quantity = float(elem [ 'Quantity' ])
        got_line.Id_HH = int(elem['RowId'])
        got_line.Price = float(elem['Price'])
        got_line.Summa = got_line.Price * got_line.Quantity
        sum_doc = sum_doc + got_line.Summa
        got_line.Department = elem['Storage']
        got_line.DepartmentCode = elem['StorageCode']
        got_line.save()

        current_line = model_to_dict(got_line)

        current_line.update(departments = departments)
        delivery_lines.append(current_line)

    delivery_order.Summa = sum_doc
    delivery_order.save()

    context = {
        'action' : '/list_delivery_order/',
        'order' : delivery_order,
        'lines' : delivery_lines,
        'order_srt' : str(delivery_order),
        'current_language' : request.session.get(LANGUAGE_SESSION_KEY),
    }
    request.session [ 'delivery_order_id' ] = delivery_order.id

    return render(request, 'edit_delivery_order.html', context)

def ajax_change_line_department(request):

    if request.is_ajax():
        line_id = request.GET.get('line_id')
        department_str = request.GET.get('department')
        department = dict(ast.literal_eval(department_str))
        DeliveryOrderLines.objects.filter(id=line_id).update(Department = department['Storage'])

        return JsonResponse({
            'status' : True
        })

def postpone(request, order_id):

    if not request.user.is_authenticated:
        return redirect('/')

    client = ClientUsers.objects.get(UserId_id=request.user.id).Client
    delivery_order = DeliveryOrders.objects.get(id = order_id)
    delivery_lines = DeliveryOrderLines.objects.filter(DeliveryOrder=delivery_order)

    clientWS = suds_client.Client(PATH_2_SERVIСES, username=WEB_USER, password=WEB_USER_PASSWORD)

    stings = []
    number = 0

    for line in delivery_lines :
        number = number + 1

        stings.append(
        {
            "Row" : str(number),
            "RowId" : str((line.id if line.Id_HH == 0 else line.Id_HH) % 100000),
            "Article" : line.Article,
            "Unit" : "",
            "Quantity" : str(line.Quantity),
            "Storage" : line.Department,
        })

    req_dic = {
        "EDRPOU" : client.EDRPOU,
        "UserName" : "",
        "OrderNum" : order_id,
        "OrderDate" : delivery_order.SysDate.strftime("%Y%m%d"),
        "Type" : "2",
        "Strings" : stings,
        "CP" : "1" if delivery_order.FormOfPayment == "CP" else "0",
    }

    try :
        result = clientWS.service.PostDeliveryOrder(client.EDRPOU, client.Password, str(req_dic))

        answer_dict0 = json.loads(str(result))
        answer_list = answer_dict0 [ 'Strings' ]

        delivery_order.Status = "order"
        delivery_order.Loaded = True

        delivery_order.save()

        delivery_lines = []

        sum_doc = 0

        for elem in answer_list :

            got_line = DeliveryOrderLines.objects.get(DeliveryOrder=delivery_order, Id_HH = elem['RowId'])

            got_line.Quantity = float(elem [ 'Quantity' ])
            got_line.Id_HH = int(elem['RowId'])
            got_line.Price = float(elem['Price'])
            got_line.Summa = got_line.Price * got_line.Quantity
            sum_doc = sum_doc + got_line.Summa
            got_line.Department = elem['Storage']
            got_line.DepartmentCode = elem['StorageCode']
            got_line.save()

            departments = [ {'Storage' : elem['Storage'], 'Quantity' : ""}, ]

            current_line = model_to_dict(got_line)

            current_line.update(departments = departments)
            delivery_lines.append(current_line)

        delivery_order.Summa = sum_doc
        delivery_order.save()

    except :
        pass

    context = {
        'action' : '/list_delivery_order/',
        'order' : delivery_order,
        'lines' : delivery_lines,
        'order_srt' : str(delivery_order),
        'current_language' : request.session.get(LANGUAGE_SESSION_KEY),
    }
    request.session [ 'delivery_order_id' ] = delivery_order.id

    return render(request, 'edit_delivery_order.html', context)


def ready_to_work(request, order_id):

    if not request.user.is_authenticated:
        return redirect('/')

    client = ClientUsers.objects.get(UserId_id=request.user.id).Client
    delivery_order = DeliveryOrders.objects.get(id = order_id)
    delivery_lines = DeliveryOrderLines.objects.filter(DeliveryOrder=delivery_order)

    clientWS = suds_client.Client(PATH_2_SERVIСES, username=WEB_USER, password=WEB_USER_PASSWORD)

    stings = []
    number = 0

    for line in delivery_lines :
        number = number + 1

        stings.append(
        {
            "Row" : str(number),
            "RowId" : str((line.id if line.Id_HH == 0 else line.Id_HH) % 100000),
            "Article" : line.Article,
            "Unit" : "",
            "Quantity" : str(line.Quantity),
            "Storage" : line.Department,
        })

    req_dic = {
        "EDRPOU" : client.EDRPOU,
        "UserName" : "",
        "OrderNum" : order_id,
        "OrderDate" : delivery_order.SysDate.strftime("%Y%m%d"),
        "Type" : "3",
        "Strings" : stings,
        "CP" : "1" if delivery_order.FormOfPayment == "CP" else "0",
    }

    req_dic.update(City="")
    req_dic.update(Recipient="")
    req_dic.update(Person="")
    req_dic.update(Phone="")

    if delivery_order.Shipping == 5 :
        req_dic.update(Shipping="5")
    if delivery_order.Shipping == 1 :
        req_dic.update(Shipping="1")
        req_dic.update(City=delivery_order.City)
        req_dic.update(Recipient=delivery_order.Recipient)
        req_dic.update(Person=delivery_order.Person)
        req_dic.update(Phone=delivery_order.Phone)

    try :

        result = clientWS.service.PostDeliveryOrder(client.EDRPOU, client.Password, str(req_dic))

        answer_dict0 = json.loads(str(result))
        answer_list = answer_dict0 [ 'Strings' ]

        delivery_order.ApprovalStatus = 1
        delivery_order.save()

    except :
        answer_list = []

    try:
        errormessage = ugettext(str(answer_dict0 [ 'Restriction' ]))

    except :
        errormessage = None


    delivery_lines = []

    for elem in answer_list :

        got_line = DeliveryOrderLines.objects.get(DeliveryOrder=delivery_order,Id_HH = elem['RowId'])

        got_line.Quantity = float(elem [ 'Quantity' ])
        got_line.Id_HH = int(elem['RowId'])
        got_line.save()

        departments = [ {'Storage' : got_line.Department, 'Quantity' : ""}, ]

        current_line = model_to_dict(got_line)

        current_line.update(departments = departments)
        delivery_lines.append(current_line)

    return list_delivery_order(request,errormessage = errormessage)



def get_delivery_pre_bill(request, order_id) :
    if not request.user.is_authenticated :
        return redirect('/')

    client = ClientUsers.objects.get(UserId_id=request.user.id).Client
    delivery_order = DeliveryOrders.objects.get(id=order_id)
    delivery_lines = DeliveryOrderLines.objects.filter(DeliveryOrder=delivery_order)

    clientWS = suds_client.Client(PATH_2_SERVIСES, username=WEB_USER, password=WEB_USER_PASSWORD)

    stings = [ ]
    number = 0

    for line in delivery_lines :
        number = number + 1

        stings.append(
            {
                "Row" : str(number),
                "RowId" : str((line.id if line.Id_HH == 0 else line.Id_HH) % 100000),
                "Article" : line.Article,
                "Unit" : "",
                "Quantity" : str(line.Quantity),
            })

    req_dic = {
        "EDRPOU" : client.EDRPOU,
        "UserName" : "",
        "OrderNum" : order_id,
        "OrderDate" : delivery_order.SysDate.strftime("%Y%m%d"),
        "Type" : "4",
        "Strings" : stings,
        "CP" : "1" if delivery_order.FormOfPayment == "CP" else "0",
    }

    try :
        result = clientWS.service.PostDeliveryOrder(client.EDRPOU, client.Password, str(req_dic))

        answer_dict0 = json.loads(str(result))
        answer = answer_dict0['Data']

        FileType = answer_dict0['FileType']

        buffer = BytesIO.BytesIO()
        content = base64.b64decode(answer)
        buffer.write(content)

        if FileType.upper() == 'PDF' :
            response = HttpResponse(
                buffer.getvalue(),
                content_type="application/pdf",
            )
            response [ 'Content-Disposition' ] = 'inline;filename=some_file.pdf'

        if FileType.upper() in ('XLS', 'XLS95', 'XLS97', 'XLSX'):
            response = HttpResponse(
                buffer.getvalue(),
                content_type="application/vnd.ms-excel",
            )
            response [ 'Content-Disposition' ] = 'inline;filename=some_file.xls'

        return response

    except Exception as e:
        return HttpResponse("Get bill:" + str(e))

    #return HttpResponse("CHECK ORDER " + str(result))
    return list_order(request)

def get_delivery_bill(request, order_id) :
    if not request.user.is_authenticated:
        return redirect('/')

    delivery_order_HH = DeliveryOrdersHH.objects.get(id=order_id)

    try:
        client = suds_client.Client(PATH_2_SERVIСES, username = WEB_USER, password = WEB_USER_PASSWORD)

        req_text = {
            "EDRPOU" : delivery_order_HH.DeliveryOrder.Client.EDRPOU,
            "BillNum" : delivery_order_HH.BillNumber,
            "UserName" : request.user.first_name,
            "BillDate" : delivery_order_HH.BillDate.strftime("%Y%m%d"),
        }

        result = client.service.GetBill(delivery_order_HH.DeliveryOrder.Client.EDRPOU, delivery_order_HH.DeliveryOrder.Client.Password, str(req_text))
        answer_dict0 = json.loads(str(result))
        answer = answer_dict0['Data']

        FileType = answer_dict0['FileType']

        buffer = BytesIO.BytesIO()
        content = base64.b64decode(answer)
        buffer.write(content)

        if FileType.upper() == 'PDF' :
            response = HttpResponse(
                buffer.getvalue(),
                content_type="application/pdf",
            )
            response [ 'Content-Disposition' ] = 'inline;filename=some_file.pdf'

        if FileType.upper() in ('XLS', 'XLS95', 'XLS97', 'XLSX'):
            response = HttpResponse(
                buffer.getvalue(),
                content_type="application/vnd.ms-excel",
            )
            response [ 'Content-Disposition' ] = 'inline;filename=some_file.xls'

        return response

    except Exception as e:
        return HttpResponse("Get bill:" + str(e))

    delivery_lines = DeliveryOrderLines.objects.filter(DeliveryOrder_id=delivery_order_HH.DeliveryOrder.id)

    context = {
        'action' : '/list_delivery_order/',
        'order' : delivery_order_HH.DeliveryOrder,
        'lines' : delivery_lines,
        'order_srt' : str(delivery_order_HH.DeliveryOrder),
        'current_language' : request.session.get(LANGUAGE_SESSION_KEY),
    }
    request.session [ 'delivery_order_id' ] = delivery_order.id

    return render(request, 'edit_delivery_order.html', context)


def get_delivery_invoice(request, order_id) :
    if not request.user.is_authenticated:
        return redirect('/')

    delivery_order_HH = DeliveryOrdersHH.objects.get(id=order_id)

    try:
        client = suds_client.Client(PATH_2_SERVIСES, username = WEB_USER, password = WEB_USER_PASSWORD)

        req_text = {
            "EDRPOU" : delivery_order_HH.DeliveryOrder.Client.EDRPOU,
            "OrderNum" : delivery_order_HH.Number,
            "UserName" : request.user.first_name,
            "OrderDate" : delivery_order_HH.Date.strftime("%Y%m%d"),
        }

        result = client.service.GetInvoice(delivery_order_HH.DeliveryOrder.Client.EDRPOU, delivery_order_HH.DeliveryOrder.Client.Password, str(req_text))
        answer_dict0 = json.loads(str(result))
        answer = answer_dict0['Data']

        FileType = answer_dict0 [ 'FileType' ]

        buffer = BytesIO.BytesIO()
        content = base64.b64decode(answer)
        buffer.write(content)

        if FileType.upper() == 'PDF' :
            response = HttpResponse(
                buffer.getvalue(),
                content_type="application/pdf",
            )
            response [ 'Content-Disposition' ] = 'inline;filename=some_file.pdf'

        if FileType.upper() in ('XLS', 'XLS95', 'XLS97', 'XLSX'):
            response = HttpResponse(
                buffer.getvalue(),
                content_type="application/vnd.ms-excel",
            )
            response [ 'Content-Disposition' ] = 'inline;filename=some_file.xls'

        return response

    except Exception as e:
        return HttpResponse("Get invoice:" + str(e))

    delivery_lines = DeliveryOrderLines.objects.filter(DeliveryOrder_id=delivery_order_HH.DeliveryOrder.id)

    context = {
        'action' : '/list_delivery_order/',
        'order' : delivery_order_HH.DeliveryOrder,
        'lines' : delivery_lines,
        'order_srt' : str(delivery_order_HH.DeliveryOrder),
        'current_language' : request.session.get(LANGUAGE_SESSION_KEY),
    }
    request.session [ 'delivery_order_id' ] = delivery_order.id

    return render(request, 'edit_delivery_order.html', context)


def reject_delivery_order(request, order_id) :
    if not request.user.is_authenticated:
        return redirect('/')

    delivery_order = DeliveryOrders.objects.get(id=order_id)

    try:
        client = suds_client.Client(PATH_2_SERVIСES, username = WEB_USER, password = WEB_USER_PASSWORD)

        req_text = {
            "EDRPOU" : delivery_order.Client.EDRPOU,
            "OrderNum" : str(delivery_order.id),
            "UserName" : request.user.first_name,
            "OrderDate" : delivery_order.SysDate.strftime("%Y%m%d"),
            "Message" : "Reject",
            "Delivery" : "1",
            "PartnerOrderNum" : str(delivery_order.id),
            "PartnerOrderDate" : delivery_order.SysDate.strftime("%Y%m%d"),
        }

        result = client.service.ConfirmSealOrder(delivery_order.Client.EDRPOU, delivery_order.Client.Password, str(req_text))

        if str(result) == "The order is successfully rejected":
            DeliveryOrders.objects.filter(id=order_id).update(ApprovalStatus = -1)
    except:
        pass

    return list_delivery_order(request)


def copy_delivery_line(request, line_id):

    if not request.user.is_authenticated:
        return redirect('/')

    line = DeliveryOrderLines.objects.get(id = line_id)
    line.id = None
    line.save()

    delivery_order = DeliveryOrders.objects.get(id = line.DeliveryOrder.id)
    delivery_lines = DeliveryOrderLines.objects.filter(DeliveryOrder_id=delivery_order.id)

    doc_sum = 0
    for delivery_line in delivery_lines:
        doc_sum = doc_sum + delivery_line.Summa

    delivery_order.Summa = doc_sum
    delivery_order.save()

    context = {
        'action' : '/list_delivery_order/',
        'form' : DeliveryOrderForm(),
        'order' : delivery_order,
        'lines' : delivery_lines,
        'order_srt' : str(delivery_order),
        'current_language' : request.session.get(LANGUAGE_SESSION_KEY),
        'form_of_payment' : delivery_order.FormOfPayment }

    return render(request, 'edit_delivery_order.html', context)

